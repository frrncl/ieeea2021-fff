%% compute_sig_tests
% 
% Computes different types of significance test, counts of significance
% chances between measures and rank measures, and saves the counts to a
% |.mat| file.
%
%% Synopsis
%
%   [] = compute_sig_tests(trackID, startMeasure, endMeasure)
%  
%
% *Parameters*
%
% * *|trackID|* - the identifier of the track to process.
% * *|startMeasure|* - the index of the start measure to process. Optional.
% * *|endMeasure|* - the index of the end measure to process. Optional.
%
% *Returns*
%
% Nothing
%

%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2017a or higher
% * *Copyright:* (C) 2018-2021 <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

function [] = compute_sig_tests(trackID, startMeasure, endMeasure)

    % check the number of input arguments
    narginchk(1, 3);

    % setup common parameters
    common_parameters;
        
    % check that trackID is a non-empty string
    validateattributes(trackID,{'char', 'cell'}, {'nonempty', 'vector'}, '', 'trackID');

    if iscell(trackID)
        % check that trackID is a cell array of strings with one element
        assert(iscellstr(trackID) && numel(trackID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected trackID to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    trackID = char(strtrim(trackID));
    trackID = trackID(:).';
    
    % check that trackID assumes a valid value
    validatestring(trackID, ...
        EXPERIMENT.track.list, '', 'trackID');
  
    if nargin == 3
        validateattributes(startMeasure, {'numeric'}, ...
            {'nonempty', 'integer', 'scalar', '>=', 1, '<=', EXPERIMENT.measure.number }, '', 'startMeasure');
        
        validateattributes(endMeasure, {'numeric'}, ...
            {'nonempty', 'integer', 'scalar', '>=', startMeasure, '<=', EXPERIMENT.measure.number }, '', 'endMeasure');
    else
        startMeasure = 1;
        endMeasure = EXPERIMENT.measure.number - EXPERIMENT.measure.helperNumber;
    end
    
    % start of overall computations
    startComputation = tic;
    
    fprintf('\n\n######## Computing significance tests on track %s (%s) ########\n\n', EXPERIMENT.track.(trackID).name, EXPERIMENT.label.paper);
    
    fprintf('+ Settings\n');
    fprintf('  - computed on %s\n', datestr(now, 'yyyy-mm-dd at HH:MM:SS'));
    fprintf('  - slice \n');
    fprintf('    * start measure: %d (%s)\n', startMeasure, EXPERIMENT.measure.getAcronym(startMeasure));
    fprintf('    * end measure: %d (%s)\n\n', endMeasure, EXPERIMENT.measure.getAcronym(endMeasure));
    
    % for each type of tie breaking
    for tt = 1:EXPERIMENT.ties.number
        ties = EXPERIMENT.ties.list{tt};
        
        fprintf('+ Breaking ties with %s\n', ties);
        % for each measure
        for m = startMeasure:endMeasure
            
            start = tic;
            
            fprintf('  - analysing %s\n', EXPERIMENT.measure.getAcronym(m));
            
            mid = EXPERIMENT.measure.list{m};
            
            measureID = EXPERIMENT.pattern.identifier.measure(mid, trackID);
            
            serload2(EXPERIMENT.pattern.file.measure(trackID, measureID), ...
                'WorkspaceVarNames', {'msr'}, ...
                'FileVarNames', {measureID});
            
            measureID = EXPERIMENT.pattern.identifier.rankMeasure(mid, ties, trackID);
            
            serload2(EXPERIMENT.pattern.file.measure(trackID, measureID), ...
                'WorkspaceVarNames', {'rnkmsr'}, ...
                'FileVarNames', {measureID});
            
            % ensure that the systems are in the same order (it should already
            % be the case)
            rnkmsr = rnkmsr(:, msr.Properties.VariableNames);
            
            % all the possible pairs of system comparisons
            pairs = nchoosek(1:width(rnkmsr), 2);
            
            % the different counts recorded
            % # cnt.pair - the total number of system pairs compares
            %
            % for each test (tid - test id)
            % # cnt.(tid).msr.sig - the total number of significantly different
            % systems using the measure
            % # cnt.(tid).msr.notsig - the total number of not significantly
            % different systems using the measure
            % # cnt.(tid).rnkmsr.sig - the total number of significantly
            % different systems using the rank measure
            % # cnt.(tid).rnkmsr.notsig - the total number of not significantly
            % different systems using the rank measure
            % # cnt.(tid).sig2notsig - number of pairs changed from
            % significantly to not significantly different when passing from
            % the measure to the rank measure
            % # cnt.(tid).notsig2sig - number of pairs changed from
            % not significantly to significantly different when passing from
            % the measure to the rank measure
            % # cnt.(tid).sig - number of pairs which are significantly
            % different for both the measure and the rank measure
            % # cnt.(tid).notsig - number of pairs which are not significantly
            % different for both the measure and the rank measure
            % # cnt.(tid).pInc - number of pairs for which the p-value has
            % increased when passing from the measure to the rank measure
            % # cnt.(tid).pDec - number of pairs for which the p-value has
            % decreased when passing from the measure to the rank measure
            % # cnt.(tid).pEq - number of pairs for which the p-value stayed
            % equal when passing from the measure to the rank measure
            
            % general counts
            cnt.pairs = size(pairs, 1);
            
            % counts for the sign test
            cnt.sgn.msr.sig = 0;
            cnt.sgn.msr.notsig = 0;
            
            cnt.sgn.rnkmsr.sig = 0;
            cnt.sgn.rnkmsr.notsig = 0;
            
            cnt.sgn.sig2notsig = 0;
            cnt.sgn.notsig2sig = 0;
            cnt.sgn.sig2sig = 0;
            cnt.sgn.notsig2notsig = 0;
            
            cnt.sgn.pInc = 0;
            cnt.sgn.pDec = 0;
            cnt.sgn.pEq = 0;
            
            % counts for the Wilcoxon signed rank test
            cnt.sgnrnk.msr.sig = 0;
            cnt.sgnrnk.msr.notsig = 0;
            
            cnt.sgnrnk.rnkmsr.sig = 0;
            cnt.sgnrnk.rnkmsr.notsig = 0;
            
            cnt.sgnrnk.sig2notsig = 0;
            cnt.sgnrnk.notsig2sig = 0;
            cnt.sgnrnk.sig2sig = 0;
            cnt.sgnrnk.notsig2notsig = 0;
            
            cnt.sgnrnk.pInc = 0;
            cnt.sgnrnk.pDec = 0;
            cnt.sgnrnk.pEq = 0;
            
            % counts for the Wilcoxon rank sum test
            cnt.rnksum.msr.sig = 0;
            cnt.rnksum.msr.notsig = 0;
            
            cnt.rnksum.rnkmsr.sig = 0;
            cnt.rnksum.rnkmsr.notsig = 0;
            
            cnt.rnksum.sig2notsig = 0;
            cnt.rnksum.notsig2sig = 0;
            cnt.rnksum.sig2sig = 0;
            cnt.rnksum.notsig2notsig = 0;
            
            cnt.rnksum.pInc = 0;
            cnt.rnksum.pDec = 0;
            cnt.rnksum.pEq = 0;
            
            % counts for the Student's t test
            cnt.t.msr.sig = 0;
            cnt.t.msr.notsig = 0;
            
            cnt.t.rnkmsr.sig = 0;
            cnt.t.rnkmsr.notsig = 0;
            
            cnt.t.sig2notsig = 0;
            cnt.t.notsig2sig = 0;
            cnt.t.sig2sig = 0;
            cnt.t.notsig2notsig = 0;
            
            cnt.t.pInc = 0;
            cnt.t.pDec = 0;
            cnt.t.pEq = 0;
            
            % counts for the one-way ANOVA for the System Effect
            cnt.anova1.msr.sig = 0;
            cnt.anova1.msr.notsig = 0;
            
            cnt.anova1.rnkmsr.sig = 0;
            cnt.anova1.rnkmsr.notsig = 0;
            
            cnt.anova1.sig2notsig = 0;
            cnt.anova1.notsig2sig = 0;
            cnt.anova1.sig2sig = 0;
            cnt.anova1.notsig2notsig = 0;
            
            cnt.anova1.pInc = 0;
            cnt.anova1.pDec = 0;
            cnt.anova1.pEq = 0;
            
            % counts for the Kruskal-Wallis Test for the System Effect
            cnt.kw.msr.sig = 0;
            cnt.kw.msr.notsig = 0;
            
            cnt.kw.rnkmsr.sig = 0;
            cnt.kw.rnkmsr.notsig = 0;
            
            cnt.kw.sig2notsig = 0;
            cnt.kw.notsig2sig = 0;
            cnt.kw.sig2sig = 0;
            cnt.kw.notsig2notsig = 0;
            
            cnt.kw.pInc = 0;
            cnt.kw.pDec = 0;
            cnt.kw.pEq = 0;
            
            % counts for the two-way ANOVA for the Topic and System Effects
            cnt.anova2.msr.sig = 0;
            cnt.anova2.msr.notsig = 0;
            
            cnt.anova2.rnkmsr.sig = 0;
            cnt.anova2.rnkmsr.notsig = 0;
            
            cnt.anova2.sig2notsig = 0;
            cnt.anova2.notsig2sig = 0;
            cnt.anova2.sig2sig = 0;
            cnt.anova2.notsig2notsig = 0;
            
            cnt.anova2.pInc = 0;
            cnt.anova2.pDec = 0;
            cnt.anova2.pEq = 0;
            
            % counts for the Friedman Test for the Topic and System Effects
            cnt.f.msr.sig = 0;
            cnt.f.msr.notsig = 0;
            
            cnt.f.rnkmsr.sig = 0;
            cnt.f.rnkmsr.notsig = 0;
            
            cnt.f.sig2notsig = 0;
            cnt.f.notsig2sig = 0;
            cnt.f.sig2sig = 0;
            cnt.f.notsig2notsig = 0;
            
            cnt.f.pInc = 0;
            cnt.f.pDec = 0;
            cnt.f.pEq = 0;
            
            tmp1 = 0;
            tmp2 = 0;
            tmp3 = 0;
            
            
            fprintf('    * performing paired sign test, signed rank test, rank sum test, and t test\n');
            for i = 1:cnt.pairs
                
                p_msr = EXPERIMENT.analysis.sgn.compute(msr{:, pairs(i, 1)} , msr{:, pairs(i, 2)});
                
                if(isnan(p_msr))
                    fprintf('      # sign test: NaN p_msr for pair %d (%d, %d), setting it to 1.0 \n', i, pairs(i, 1), pairs(i, 2));
                    p_msr = 1.0;
                end
                
                msrsig = p_msr <= EXPERIMENT.analysis.alpha.threshold;
                
                
                p_rnkmsr = EXPERIMENT.analysis.sgn.compute(rnkmsr{:, pairs(i, 1)} , rnkmsr{:, pairs(i, 2)});
                
                if(isnan(p_rnkmsr))
                    fprintf('      # sign test: NaN p_rnkmsr for pair %d (%d, %d), setting it to 1.0 \n', i, pairs(i, 1), pairs(i, 2));
                    p_rnkmsr = 1.0;
                end
                
                rnkmsrsig = p_rnkmsr <= EXPERIMENT.analysis.alpha.threshold;
                
                cnt.sgn.msr.sig = cnt.sgn.msr.sig + msrsig;
                cnt.sgn.msr.notsig = cnt.sgn.msr.notsig + (~msrsig);
                
                cnt.sgn.rnkmsr.sig = cnt.sgn.rnkmsr.sig + rnkmsrsig;
                cnt.sgn.rnkmsr.notsig = cnt.sgn.rnkmsr.notsig + (~rnkmsrsig);
                
                cnt.sgn.sig2notsig = cnt.sgn.sig2notsig + (msrsig && ~rnkmsrsig);
                cnt.sgn.notsig2sig = cnt.sgn.notsig2sig + (~msrsig && rnkmsrsig);
                cnt.sgn.sig2sig = cnt.sgn.sig2sig + (msrsig && rnkmsrsig);
                cnt.sgn.notsig2notsig = cnt.sgn.notsig2notsig + (~msrsig && ~rnkmsrsig);
                
                diffeps = eps(p_msr) + eps(p_rnkmsr);
                cnt.sgn.pInc = cnt.sgn.pInc + (p_rnkmsr - p_msr > diffeps);
                cnt.sgn.pDec = cnt.sgn.pDec + (p_msr - p_rnkmsr > diffeps);
                cnt.sgn.pEq = cnt.sgn.pEq + (abs(p_rnkmsr - p_msr) <= diffeps);
                
                %--- END SIGN
                
                p_msr = EXPERIMENT.analysis.sgnrnk.compute(msr{:, pairs(i, 1)} , msr{:, pairs(i, 2)});
                
                if(isnan(p_msr))
                    fprintf('      # signed rank test: NaN p_msr for pair %d (%d, %d), setting it to 1.0 \n', i, pairs(i, 1), pairs(i, 2));
                    p_msr = 1.0;
                end
                
                msrsig = p_msr <= EXPERIMENT.analysis.alpha.threshold;
                
                
                p_rnkmsr = EXPERIMENT.analysis.sgnrnk.compute(rnkmsr{:, pairs(i, 1)} , rnkmsr{:, pairs(i, 2)});
                
                if(isnan(p_rnkmsr))
                    fprintf('      # signed rank test: NaN p_rnkmsr for pair %d (%d, %d), setting it to 1.0 \n', i, pairs(i, 1), pairs(i, 2));
                    p_rnkmsr = 1.0;
                end
                
                rnkmsrsig = p_rnkmsr <= EXPERIMENT.analysis.alpha.threshold;
                
                cnt.sgnrnk.msr.sig = cnt.sgnrnk.msr.sig + msrsig;
                cnt.sgnrnk.msr.notsig = cnt.sgnrnk.msr.notsig + (~msrsig);
                
                cnt.sgnrnk.rnkmsr.sig = cnt.sgnrnk.rnkmsr.sig + rnkmsrsig;
                cnt.sgnrnk.rnkmsr.notsig = cnt.sgnrnk.rnkmsr.notsig + (~rnkmsrsig);
                
                cnt.sgnrnk.sig2notsig = cnt.sgnrnk.sig2notsig + (msrsig && ~rnkmsrsig);
                cnt.sgnrnk.notsig2sig = cnt.sgnrnk.notsig2sig + (~msrsig && rnkmsrsig);
                cnt.sgnrnk.sig2sig = cnt.sgnrnk.sig2sig + (msrsig && rnkmsrsig);
                cnt.sgnrnk.notsig2notsig = cnt.sgnrnk.notsig2notsig + (~msrsig && ~rnkmsrsig);
                
                diffeps = eps(p_msr) + eps(p_rnkmsr);
                cnt.sgnrnk.pInc = cnt.sgnrnk.pInc + (p_rnkmsr - p_msr > diffeps);
                cnt.sgnrnk.pDec = cnt.sgnrnk.pDec + (p_msr - p_rnkmsr > diffeps);
                cnt.sgnrnk.pEq = cnt.sgnrnk.pEq + (abs(p_rnkmsr - p_msr) <= diffeps);
                
                % ---- END SIGNRNK
                
                
                p_msr = EXPERIMENT.analysis.rnksum.compute(msr{:, pairs(i, 1)} , msr{:, pairs(i, 2)});
                
                if(isnan(p_msr))
                    fprintf('      # rank sum test: NaN p_msr for pair %d (%d, %d), setting it to 1.0 \n', i, pairs(i, 1), pairs(i, 2));
                    p_msr = 1.0;
                end
                
                msrsig = p_msr <= EXPERIMENT.analysis.alpha.threshold;
                
                
                p_rnkmsr = EXPERIMENT.analysis.rnksum.compute(rnkmsr{:, pairs(i, 1)} , rnkmsr{:, pairs(i, 2)});
                
                if(isnan(p_rnkmsr))
                    fprintf('      # rank sum test: NaN p_rnkmsr for pair %d (%d, %d), setting it to 1.0 \n', i, pairs(i, 1), pairs(i, 2));
                    p_rnkmsr = 1.0;
                end
                
                rnkmsrsig = p_rnkmsr <= EXPERIMENT.analysis.alpha.threshold;
                
                cnt.rnksum.msr.sig = cnt.rnksum.msr.sig + msrsig;
                cnt.rnksum.msr.notsig = cnt.rnksum.msr.notsig + (~msrsig);
                
                cnt.rnksum.rnkmsr.sig = cnt.rnksum.rnkmsr.sig + rnkmsrsig;
                cnt.rnksum.rnkmsr.notsig = cnt.rnksum.rnkmsr.notsig + (~rnkmsrsig);
                
                cnt.rnksum.sig2notsig = cnt.rnksum.sig2notsig + (msrsig && ~rnkmsrsig);
                cnt.rnksum.notsig2sig = cnt.rnksum.notsig2sig + (~msrsig && rnkmsrsig);
                cnt.rnksum.sig2sig = cnt.rnksum.sig2sig + (msrsig && rnkmsrsig);
                cnt.rnksum.notsig2notsig = cnt.rnksum.notsig2notsig + (~msrsig && ~rnkmsrsig);
                
                diffeps = eps(p_msr) + eps(p_rnkmsr);
                cnt.rnksum.pInc = cnt.rnksum.pInc + (p_rnkmsr - p_msr > diffeps);
                cnt.rnksum.pDec = cnt.rnksum.pDec + (p_msr - p_rnkmsr > diffeps);
                cnt.rnksum.pEq = cnt.rnksum.pEq + (abs(p_rnkmsr - p_msr) <= diffeps);
                
                % ---- END RNKSUM
                
                [~, p_msr] = EXPERIMENT.analysis.t.compute(msr{:, pairs(i, 1)} , msr{:, pairs(i, 2)});
                
                if(isnan(p_msr))
                    fprintf('      # t test: NaN p_msr for pair %d (%d, %d), setting it to 1.0 \n', i, pairs(i, 1), pairs(i, 2));
                    p_msr = 1.0;
                end
                
                msrsig = p_msr <= EXPERIMENT.analysis.alpha.threshold;
                
                
                [~, p_rnkmsr] = EXPERIMENT.analysis.t.compute(rnkmsr{:, pairs(i, 1)} , rnkmsr{:, pairs(i, 2)});
                
                if(isnan(p_rnkmsr))
                    fprintf('      # t test: NaN p_rnkmsr for pair %d (%d, %d), setting it to 1.0 \n', i, pairs(i, 1), pairs(i, 2));
                    p_rnkmsr = 1.0;
                end
                
                rnkmsrsig = p_rnkmsr <= EXPERIMENT.analysis.alpha.threshold;
                
                cnt.t.msr.sig = cnt.t.msr.sig + msrsig;
                cnt.t.msr.notsig = cnt.t.msr.notsig + (~msrsig);
                
                cnt.t.rnkmsr.sig = cnt.t.rnkmsr.sig + rnkmsrsig;
                cnt.t.rnkmsr.notsig = cnt.t.rnkmsr.notsig + (~rnkmsrsig);
                
                cnt.t.sig2notsig = cnt.t.sig2notsig + (msrsig && ~rnkmsrsig);
                cnt.t.notsig2sig = cnt.t.notsig2sig + (~msrsig && rnkmsrsig);
                cnt.t.sig2sig = cnt.t.sig2sig + (msrsig && rnkmsrsig);
                cnt.t.notsig2notsig = cnt.t.notsig2notsig + (~msrsig && ~rnkmsrsig);
                
                diffeps = eps(p_msr) + eps(p_rnkmsr);
                cnt.t.pInc = cnt.t.pInc + (p_rnkmsr - p_msr > diffeps);
                cnt.t.pDec = cnt.t.pDec + (p_msr - p_rnkmsr > diffeps);
                cnt.t.pEq = cnt.t.pEq + (abs(p_rnkmsr - p_msr) <= diffeps);
                
                if tmp1 == cnt.t.pInc && tmp2 == cnt.t.pDec && tmp3 == cnt.t.pEq
                    fprintf('      # t test pair %d (%d, %d) p-value changes not work \n', i, pairs(i, 1), pairs(i, 2));
                    fprintf('      # t test (p_rnkmsr - p_msr > diffeps) = %d \n', (p_rnkmsr - p_msr > diffeps));
                    fprintf('      # t test (p_msr - p_rnkmsr > diffeps) = %d \n', (p_msr - p_rnkmsr > diffeps));
                    fprintf('      # t test (abs(p_rnkmsr - p_msr) <= diffeps) = %d \n', (abs(p_rnkmsr - p_msr) <= diffeps));
                    fprintf('      # t test: p_msr %f, p_rnkmsr %f \n', p_msr, p_rnkmsr);
                end
                
                tmp1 = cnt.t.pInc;
                tmp2 = cnt.t.pDec;
                tmp3 = cnt.t.pEq;
                
                % ---- END t
                
            end
            
            assert(cnt.pairs == cnt.sgn.rnkmsr.sig + cnt.sgn.rnkmsr.notsig, 'Sign test, msr sig counts are not equal: %d vs %d', cnt.pairs, cnt.sgn.msr.sig + cnt.sgn.msr.notsig);
            assert(cnt.pairs == cnt.sgn.rnkmsr.sig + cnt.sgn.rnkmsr.notsig, 'Sign test, rnkmsr sig counts are not equal: %d vs %d', cnt.pairs, cnt.sgn.rnkmsr.sig + cnt.sgn.rnkmsr.notsig);
            assert(cnt.pairs == cnt.sgn.sig2notsig + cnt.sgn.notsig2sig + cnt.sgn.sig2sig + cnt.sgn.notsig2notsig, 'Sign test, sig changes counts are not equal: %d vs %d', cnt.pairs, cnt.sgn.sig2notsig + cnt.sgn.notsig2sig + cnt.sgn.sig2sig + cnt.sgn.notsig2notsig);
            assert(cnt.pairs == cnt.sgn.pInc + cnt.sgn.pDec + cnt.sgn.pEq, 'Sign test, p-value changes counts are not equal: %d vs %d', cnt.pairs, cnt.sgn.pInc + cnt.sgn.pDec + cnt.sgn.pEq);
            
            assert(cnt.pairs == cnt.sgnrnk.rnkmsr.sig + cnt.sgnrnk.rnkmsr.notsig, 'Signed Rank test, msr sig counts are not equal: %d vs %d', cnt.pairs, cnt.sgnrnk.msr.sig + cnt.sgnrnk.msr.notsig);
            assert(cnt.pairs == cnt.sgnrnk.rnkmsr.sig + cnt.sgnrnk.rnkmsr.notsig, 'Signed Rank test, rnkmsr sig counts are not equal: %d vs %d', cnt.pairs, cnt.sgnrnk.rnkmsr.sig + cnt.sgnrnk.rnkmsr.notsig);
            assert(cnt.pairs == cnt.sgnrnk.sig2notsig + cnt.sgnrnk.notsig2sig + cnt.sgnrnk.sig2sig + cnt.sgnrnk.notsig2notsig, 'Signed Rank test, sig changes counts are not equal: %d vs %d', cnt.pairs, cnt.sgnrnk.sig2notsig + cnt.sgnrnk.notsig2sig + cnt.sgnrnk.sig2sig + cnt.sgnrnk.notsig2notsig);
            assert(cnt.pairs == cnt.sgnrnk.pInc + cnt.sgnrnk.pDec + cnt.sgnrnk.pEq, 'Signed Rank test, p-value changes counts are not equal: %d vs %d', cnt.pairs, cnt.sgnrnk.pInc + cnt.sgnrnk.pDec + cnt.sgnrnk.pEq);
            
            assert(cnt.pairs == cnt.rnksum.rnkmsr.sig + cnt.rnksum.rnkmsr.notsig, 'Rank Sum test, msr sig counts are not equal: %d vs %d', cnt.pairs, cnt.rnksum.msr.sig + cnt.rnksum.msr.notsig);
            assert(cnt.pairs == cnt.rnksum.rnkmsr.sig + cnt.rnksum.rnkmsr.notsig, 'Rank Sum test, rnkmsr sig counts are not equal: %d vs %d', cnt.pairs, cnt.rnksum.rnkmsr.sig + cnt.rnksum.rnkmsr.notsig);
            assert(cnt.pairs == cnt.rnksum.sig2notsig + cnt.rnksum.notsig2sig + cnt.rnksum.sig2sig + cnt.rnksum.notsig2notsig, 'Rank Sum test, sig changes counts are not equal: %d vs %d', cnt.pairs, cnt.rnksum.sig2notsig + cnt.rnksum.notsig2sig + cnt.rnksum.sig2sig + cnt.rnksum.notsig2notsig);
            assert(cnt.pairs == cnt.rnksum.pInc + cnt.rnksum.pDec + cnt.rnksum.pEq, 'Rank Sum test, p-value changes counts are not equal: %d vs %d', cnt.pairs, cnt.rnksum.pInc + cnt.rnksum.pDec + cnt.rnksum.pEq);
            
            assert(cnt.pairs == cnt.t.rnkmsr.sig + cnt.t.rnkmsr.notsig, 'Student''s t test, msr sig counts are not equal: %d vs %d', cnt.pairs, cnt.t.msr.sig + cnt.t.msr.notsig);
            assert(cnt.pairs == cnt.t.rnkmsr.sig + cnt.t.rnkmsr.notsig, 'Student''s t test, rnkmsr sig counts are not equal: %d vs %d', cnt.pairs, cnt.t.rnkmsr.sig + cnt.t.rnkmsr.notsig);
            assert(cnt.pairs == cnt.t.sig2notsig + cnt.t.notsig2sig + cnt.t.sig2sig + cnt.t.notsig2notsig, 'Student''s t test, sig changes counts are not equal: %d vs %d', cnt.pairs, cnt.t.sig2notsig + cnt.t.notsig2sig + cnt.t.sig2sig + cnt.t.notsig2notsig);
            assert(cnt.pairs == cnt.t.pInc + cnt.t.pDec + cnt.t.pEq, 'Student''s t test, p-value changes counts are not equal: %d vs %d', cnt.pairs, cnt.t.pInc + cnt.t.pDec + cnt.t.pEq);
            
            
            
            
            
            fprintf('    * performing one-way anova multiple comparisons \n');
            
            [~, ~, sts] = EXPERIMENT.analysis.anova1.compute(msr{:, :});
            mc_msr = EXPERIMENT.analysis.multcompare.system(sts);
            assert(cnt.pairs == size(mc_msr, 1), 'ANOVA1 test, pairs are not equal: %d vs %d', cnt.pairs, size(mc_msr, 1));
            
            [~, ~, sts] = EXPERIMENT.analysis.anova1.compute(rnkmsr{:, :});
            mc_rnkmsr = EXPERIMENT.analysis.multcompare.system(sts);
            
            p_msr = mc_msr(:, 6);
            
            idx = isnan(p_msr);
            if(any(idx))
                t = find(idx);
                for i = 1:length(t)
                    fprintf('      # anova1: NaN p_msr for pair %d (%d, %d), setting it to 1.0 \n', i, pairs(t(i), 1), pairs(t(i), 2));
                end
                p_msr(idx) = 1.0;
            end
            
            msrsig = p_msr <= EXPERIMENT.analysis.alpha.threshold;
            
            
            p_rnkmsr = mc_rnkmsr(:, 6);
            
            idx = isnan(p_rnkmsr);
            if(any(idx))
                t = find(idx);
                for i = 1:length(t)
                    fprintf('      # anova1: NaN p_p_rnkmsr for pair %d (%d, %d), setting it to 1.0 \n', i, pairs(t(i), 1), pairs(t(i), 2));
                end
                p_rnkmsr(idx) = 1.0;
            end
            
            rnkmsrsig = p_rnkmsr <= EXPERIMENT.analysis.alpha.threshold;
            
            cnt.anova1.msr.sig = sum(msrsig);
            cnt.anova1.msr.notsig = sum(~msrsig);
            
            cnt.anova1.rnkmsr.sig = sum(rnkmsrsig);
            cnt.anova1.rnkmsr.notsig = sum(~rnkmsrsig);
            
            cnt.anova1.sig2notsig = sum(msrsig & ~rnkmsrsig);
            cnt.anova1.notsig2sig = sum(~msrsig & rnkmsrsig);
            cnt.anova1.sig2sig = sum(msrsig & rnkmsrsig);
            cnt.anova1.notsig2notsig = sum(~msrsig & ~rnkmsrsig);
            
            diffeps = eps(p_msr) + eps(p_rnkmsr);
            cnt.anova1.pInc = sum(p_rnkmsr - p_msr > diffeps);
            cnt.anova1.pDec = sum(p_msr - p_rnkmsr > diffeps);
            cnt.anova1.pEq = sum(abs(p_rnkmsr - p_msr) <= diffeps);
            
            % ---- END ANOVA1
            
            fprintf('    * performing Kruskal-Wallis multiple comparisons \n');
            [~, ~, sts] = EXPERIMENT.analysis.kw.compute(msr{:, :});
            mc_msr = EXPERIMENT.analysis.multcompare.system(sts);
            assert(cnt.pairs == size(mc_msr, 1), 'Kruskal-Wallis test, pairs are not equal: %d vs %d', cnt.pairs, size(mc_msr, 1));
            
            [~, ~, sts] = EXPERIMENT.analysis.kw.compute(rnkmsr{:, :});
            mc_rnkmsr = EXPERIMENT.analysis.multcompare.system(sts);
            
            p_msr = mc_msr(:, 6);
            
            idx = isnan(p_msr);
            if(any(idx))
                t = find(idx);
                for i = 1:length(t)
                    fprintf('      # Kruskal-Wallis: NaN p_msr for pair %d (%d, %d), setting it to 1.0 \n', i, pairs(t(i), 1), pairs(t(i), 2));
                end
                p_msr(idx) = 1.0;
            end
            
            msrsig = p_msr <= EXPERIMENT.analysis.alpha.threshold;
            
            
            p_rnkmsr = mc_rnkmsr(:, 6);
            
            idx = isnan(p_rnkmsr);
            if(any(idx))
                t = find(idx);
                for i = 1:length(t)
                    fprintf('      # Kruskal-Wallis: NaN p_p_rnkmsr for pair %d (%d, %d), setting it to 1.0 \n', i, pairs(t(i), 1), pairs(t(i), 2));
                end
                p_rnkmsr(idx) = 1.0;
            end
            
            rnkmsrsig = p_rnkmsr <= EXPERIMENT.analysis.alpha.threshold;
            
            cnt.kw.msr.sig = sum(msrsig);
            cnt.kw.msr.notsig = sum(~msrsig);
            
            cnt.kw.rnkmsr.sig = sum(rnkmsrsig);
            cnt.kw.rnkmsr.notsig = sum(~rnkmsrsig);
            
            cnt.kw.sig2notsig = sum(msrsig & ~rnkmsrsig);
            cnt.kw.notsig2sig = sum(~msrsig & rnkmsrsig);
            cnt.kw.sig2sig = sum(msrsig & rnkmsrsig);
            cnt.kw.notsig2notsig = sum(~msrsig & ~rnkmsrsig);
            
            diffeps = eps(p_msr) + eps(p_rnkmsr);
            cnt.kw.pInc = sum(p_rnkmsr - p_msr > diffeps);
            cnt.kw.pDec = sum(p_msr - p_rnkmsr > diffeps);
            cnt.kw.pEq = sum(abs(p_rnkmsr - p_msr) <= diffeps);
            
            % ---- END Kruskal-Wallis
            
            
            fprintf('    * performing two-way anova multiple comparisons \n');
            
            [~, ~, sts] = EXPERIMENT.analysis.anova2.compute(msr{:, :});
            mc_msr = EXPERIMENT.analysis.multcompare.system(sts);
            assert(cnt.pairs == size(mc_msr, 1), 'ANOVA2 test, pairs are not equal: %d vs %d', cnt.pairs, size(mc_msr, 1));
            
            [~, ~, sts] = EXPERIMENT.analysis.anova2.compute(rnkmsr{:, :});
            mc_rnkmsr = EXPERIMENT.analysis.multcompare.system(sts);
            
            p_msr = mc_msr(:, 6);
            
            idx = isnan(p_msr);
            if(any(idx))
                t = find(idx);
                for i = 1:length(t)
                    fprintf('      # anova2: NaN p_msr for pair %d (%d, %d), setting it to 1.0 \n', i, pairs(t(i), 1), pairs(t(i), 2));
                end
                p_msr(idx) = 1.0;
            end
            
            msrsig = p_msr <= EXPERIMENT.analysis.alpha.threshold;
            
            p_rnkmsr = mc_rnkmsr(:, 6);
            
            idx = isnan(p_rnkmsr);
            if(any(idx))
                t = find(idx);
                for i = 1:length(t)
                    fprintf('      # anova2: NaN p_p_rnkmsr for pair %d (%d, %d), setting it to 1.0 \n', i, pairs(t(i), 1), pairs(t(i), 2));
                end
                p_rnkmsr(idx) = 1.0;
            end
            
            rnkmsrsig = p_rnkmsr <= EXPERIMENT.analysis.alpha.threshold;
            
            cnt.anova2.msr.sig = sum(msrsig);
            cnt.anova2.msr.notsig = sum(~msrsig);
            
            cnt.anova2.rnkmsr.sig = sum(rnkmsrsig);
            cnt.anova2.rnkmsr.notsig = sum(~rnkmsrsig);
            
            cnt.anova2.sig2notsig = sum(msrsig & ~rnkmsrsig);
            cnt.anova2.notsig2sig = sum(~msrsig & rnkmsrsig);
            cnt.anova2.sig2sig = sum(msrsig & rnkmsrsig);
            cnt.anova2.notsig2notsig = sum(~msrsig & ~rnkmsrsig);
            
            diffeps = eps(p_msr) + eps(p_rnkmsr);
            cnt.anova2.pInc = sum(p_rnkmsr - p_msr > diffeps);
            cnt.anova2.pDec = sum(p_msr - p_rnkmsr > diffeps);
            cnt.anova2.pEq = sum(abs(p_rnkmsr - p_msr) <= diffeps);
            
            % ---- END ANOVA2
            
            
            fprintf('    * performing Friedman multiple comparisons \n');
            
            [~, ~, sts] = EXPERIMENT.analysis.f.compute(msr{:, :});
            mc_msr = EXPERIMENT.analysis.multcompare.system(sts);
            assert(cnt.pairs == size(mc_msr, 1), 'Pairs are not equal: %d vs %d', cnt.pairs, size(mc_msr, 1));
            
            [~, ~, sts] = EXPERIMENT.analysis.f.compute(rnkmsr{:, :});
            mc_rnkmsr = EXPERIMENT.analysis.multcompare.system(sts);
            
            p_msr = mc_msr(:, 6);
            
            idx = isnan(p_msr);
            if(any(idx))
                t = find(idx);
                for i = 1:length(t)
                    fprintf('      # Friedman: NaN p_msr for pair %d (%d, %d), setting it to 1.0 \n', i, pairs(t(i), 1), pairs(t(i), 2));
                end
                p_msr(idx) = 1.0;
            end
            
            msrsig = p_msr <= EXPERIMENT.analysis.alpha.threshold;
            
            
            p_rnkmsr = mc_rnkmsr(:, 6);
            
            idx = isnan(p_rnkmsr);
            if(any(idx))
                t = find(idx);
                for i = 1:length(t)
                    fprintf('      # Friedman: NaN p_p_rnkmsr for pair %d (%d, %d), setting it to 1.0 \n', i, pairs(t(i), 1), pairs(t(i), 2));
                end
                p_rnkmsr(idx) = 1.0;
            end
            
            rnkmsrsig = p_rnkmsr <= EXPERIMENT.analysis.alpha.threshold;
            
            cnt.f.msr.sig = sum(msrsig);
            cnt.f.msr.notsig = sum(~msrsig);
            
            cnt.f.rnkmsr.sig = sum(rnkmsrsig);
            cnt.f.rnkmsr.notsig = sum(~rnkmsrsig);
            
            cnt.f.sig2notsig = sum(msrsig & ~rnkmsrsig);
            cnt.f.notsig2sig = sum(~msrsig & rnkmsrsig);
            cnt.f.sig2sig = sum(msrsig & rnkmsrsig);
            cnt.f.notsig2notsig = sum(~msrsig & ~rnkmsrsig);
            
            diffeps = eps(p_msr) + eps(p_rnkmsr);
            cnt.f.pInc = sum(p_rnkmsr - p_msr > diffeps);
            cnt.f.pDec = sum(p_msr - p_rnkmsr > diffeps);
            cnt.f.pEq = sum(abs(p_rnkmsr - p_msr) <= diffeps);
            
            
            % ---- END Friedman
            
            assert(cnt.pairs == cnt.anova1.rnkmsr.sig + cnt.anova1.rnkmsr.notsig, 'ANOVA1 test, msr sig counts are not equal: %d vs %d', cnt.pairs, cnt.anova1.msr.sig + cnt.anova1.msr.notsig);
            assert(cnt.pairs == cnt.anova1.rnkmsr.sig + cnt.anova1.rnkmsr.notsig, 'ANOVA1 test, rnkmsr sig counts are not equal: %d vs %d', cnt.pairs, cnt.anova1.rnkmsr.sig + cnt.anova1.rnkmsr.notsig);
            assert(cnt.pairs == cnt.anova1.sig2notsig + cnt.anova1.notsig2sig + cnt.anova1.sig2sig + cnt.anova1.notsig2notsig, 'ANOVA1 test, sig changes counts are not equal: %d vs %d', cnt.pairs, cnt.anova1.sig2notsig + cnt.anova1.notsig2sig + cnt.anova1.sig2sig + cnt.anova1.notsig2notsig);
            assert(cnt.pairs == cnt.anova1.pInc + cnt.anova1.pDec + cnt.anova1.pEq, 'ANOVA1 test, p-value changes counts are not equal: %d vs %d', cnt.pairs, cnt.anova1.pInc + cnt.anova1.pDec + cnt.anova1.pEq);
            
            assert(cnt.pairs == cnt.kw.rnkmsr.sig + cnt.kw.rnkmsr.notsig, 'Kruskal-Wallis test, msr sig counts are not equal: %d vs %d', cnt.pairs, cnt.kw.msr.sig + cnt.kw.msr.notsig);
            assert(cnt.pairs == cnt.kw.rnkmsr.sig + cnt.kw.rnkmsr.notsig, 'Kruskal-Wallis test, rnkmsr sig counts are not equal: %d vs %d', cnt.pairs, cnt.kw.rnkmsr.sig + cnt.kw.rnkmsr.notsig);
            assert(cnt.pairs == cnt.kw.sig2notsig + cnt.kw.notsig2sig + cnt.kw.sig2sig + cnt.kw.notsig2notsig, 'Kruskal-Wallis test, sig changes counts are not equal: %d vs %d', cnt.pairs, cnt.kw.sig2notsig + cnt.kw.notsig2sig + cnt.kw.sig2sig + cnt.kw.notsig2notsig);
            assert(cnt.pairs == cnt.kw.pInc + cnt.kw.pDec + cnt.kw.pEq, 'Kruskal-Wallis test, p-value changes counts are not equal: %d vs %d', cnt.pairs, cnt.kw.pInc + cnt.kw.pDec + cnt.kw.pEq);
            
            assert(cnt.pairs == cnt.anova2.rnkmsr.sig + cnt.anova2.rnkmsr.notsig, 'ANOVA2 test, msr sig counts are not equal: %d vs %d', cnt.pairs, cnt.anova2.msr.sig + cnt.anova2.msr.notsig);
            assert(cnt.pairs == cnt.anova2.rnkmsr.sig + cnt.anova2.rnkmsr.notsig, 'ANOVA2 test, rnkmsr sig counts are not equal: %d vs %d', cnt.pairs, cnt.anova2.rnkmsr.sig + cnt.anova2.rnkmsr.notsig);
            assert(cnt.pairs == cnt.anova2.sig2notsig + cnt.anova2.notsig2sig + cnt.anova2.sig2sig + cnt.anova2.notsig2notsig, 'ANOVA2 test, sig changes counts are not equal: %d vs %d', cnt.pairs, cnt.anova2.sig2notsig + cnt.anova2.notsig2sig + cnt.anova2.sig2sig + cnt.anova2.notsig2notsig);
            assert(cnt.pairs == cnt.anova2.pInc + cnt.anova2.pDec + cnt.anova2.pEq, 'ANOVA2 test, p-value changes counts are not equal: %d vs %d', cnt.pairs, cnt.anova2.pInc + cnt.anova2.pDec + cnt.anova2.pEq);
            
            assert(cnt.pairs == cnt.f.rnkmsr.sig + cnt.f.rnkmsr.notsig, 'Friedman test, msr sig counts are not equal: %d vs %d', cnt.pairs, cnt.f.msr.sig + cnt.f.msr.notsig);
            assert(cnt.pairs == cnt.f.rnkmsr.sig + cnt.f.rnkmsr.notsig, 'Friedman test, rnkmsr sig counts are not equal: %d vs %d', cnt.pairs, cnt.f.rnkmsr.sig + cnt.f.rnkmsr.notsig);
            assert(cnt.pairs == cnt.f.sig2notsig + cnt.f.notsig2sig + cnt.f.sig2sig + cnt.f.notsig2notsig, 'Friedman test, sig changes counts are not equal: %d vs %d', cnt.pairs, cnt.f.sig2notsig + cnt.f.notsig2sig + cnt.f.sig2sig + cnt.f.notsig2notsig);
            assert(cnt.pairs == cnt.f.pInc + cnt.f.pDec + cnt.f.pEq, 'Friedman test, p-value changes counts are not equal: %d vs %d', cnt.pairs, cnt.f.pInc + cnt.f.pDec + cnt.f.pEq);
            
            
            cntID = EXPERIMENT.pattern.identifier.sigcounts(mid, ties, trackID);
            
            sersave2(EXPERIMENT.pattern.file.analysis(trackID, cntID), ...
                'WorkspaceVarNames', {'cnt'}, ...
                'FileVarNames', {cntID});
            
            fprintf('  - elapsed time: %s\n', elapsedToHourMinutesSeconds(toc(start)));
            
        end % for measure
        
    end % for ties
        
    fprintf('\n\n######## Total elapsed time for computing measures on track %s (%s): %s ########\n\n', ...
            EXPERIMENT.track.(trackID).name, EXPERIMENT.label.paper, elapsedToHourMinutesSeconds(toc(startComputation)));
end
