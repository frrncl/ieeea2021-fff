%% compute_rank_measures
% 
% Computes measures as their rank with respect to their ACR version and 
% saves them to a |.mat| file.
%
%% Synopsis
%
%   compute_rank_measures(trackID, startMeasure, endMeasure)
%
% *Parameters*
%
% * *|trackID|* - the identifier of the track to process.
% * *|startMeasure|* - the index of the start measure to process. Optional.
% * *|endMeasure|* - the index of the end measure to process. Optional.
%
% *Returns*
%
% Nothing
%

%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2017a or higher
% * *Copyright:* (C) 2018-2021 <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

function [] = compute_rank_measures(trackID, startMeasure, endMeasure)
    
    % check the number of input arguments
    narginchk(1, 3);

    % setup common parameters
    common_parameters;
        
    % check that trackID is a non-empty string
    validateattributes(trackID,{'char', 'cell'}, {'nonempty', 'vector'}, '', 'trackID');

    if iscell(trackID)
        % check that trackID is a cell array of strings with one element
        assert(iscellstr(trackID) && numel(trackID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected trackID to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    trackID = char(strtrim(trackID));
    trackID = trackID(:).';    
    
    % check that trackID assumes a valid value
    validatestring(trackID, ...
        EXPERIMENT.track.list, '', 'trackID');
    
    if nargin == 3
        validateattributes(startMeasure, {'numeric'}, ...
            {'nonempty', 'integer', 'scalar', '>=', 1, '<=', EXPERIMENT.measure.number }, '', 'startMeasure');
        
        validateattributes(endMeasure, {'numeric'}, ...
            {'nonempty', 'integer', 'scalar', '>=', startMeasure, '<=', EXPERIMENT.measure.number }, '', 'endMeasure');
    else 
        startMeasure = 1;
        endMeasure = EXPERIMENT.measure.number - EXPERIMENT.measure.helperNumber;
    end
        
    % start of overall computations
    startComputation = tic;

    fprintf('\n\n######## Computing rank measures on track %s (%s) ########\n\n', EXPERIMENT.track.(trackID).name, EXPERIMENT.label.paper);

    fprintf('+ Settings\n');
    fprintf('  - computed on %s\n', datestr(now, 'yyyy-mm-dd at HH:MM:SS'));
    fprintf('  - slice \n');
    fprintf('    * start measure: %d (%s)\n', startMeasure, EXPERIMENT.measure.getAcronym(startMeasure));
    fprintf('    * end measure: %d (%s)\n\n', endMeasure, EXPERIMENT.measure.getAcronym(endMeasure));
                             
    fprintf('+ Loading the index measure values\n\n'); 
    
    measureID = EXPERIMENT.pattern.identifier.measure(EXPERIMENT.measure.idx.id, trackID);
        
    serload2(EXPERIMENT.pattern.file.measure(trackID, measureID), ...
            'WorkspaceVarNames', {'idx'}, ...
            'FileVarNames', {measureID});   
        
    % the length of the run
    N = EXPERIMENT.track.(trackID).runLength;

    T = height(idx);
    R = width(idx);
                      
    % for each measure
    for m = startMeasure:endMeasure
                                
        start = tic;
        
        fprintf('+ Computing rank %s\n', EXPERIMENT.measure.getAcronym(m));
        
        mid = EXPERIMENT.measure.list{m};
                        
        % for each type of tie breaking
        for tt = 1:EXPERIMENT.ties.number
            ties = EXPERIMENT.ties.list{tt};
            
            fprintf('  - breaking ties with %s\n', ties);
            
            % create a template for the output measure, i.e. right topic
            % and run identifiers
            msrout = idx;
            msrout{:, :} = NaN(size(msrout{:, :}));
            
            % load the ranks
            ranksID = EXPERIMENT.pattern.identifier.acr.ranks(mid, ties, N);
            
            serload2(EXPERIMENT.pattern.file.measure(EXPERIMENT.track.ACR.id, ranksID), ...
                'WorkspaceVarNames', {'ranks'}, ...
                'FileVarNames', {ranksID});
                        
            % for each topic
            for t = 1:T
                
                % for each run
                for r = 1:R
                    
                    % for each (topic, run) pair, determine its index with
                    % respect to the ACR runs, i.e. idx{t, r}
                    %
                    % use the index of the run wrt ACR runs to determine its
                    % ranks wrt ACR runs, i.e.
                    % ranks(idx{t, r})
                    %
                    % take that value as score for the rank measure
                    msrout{t, r} = ranks(idx{t, r});
                    
                end % for run
            end % for topic
                        
            measureID = EXPERIMENT.pattern.identifier.rankMeasure(mid, ties, trackID);
            
            msrout.Properties.UserData.name = measureID;
            msrout.Properties.UserData.shortName = measureID;
            
            sersave2(EXPERIMENT.pattern.file.measure(trackID, measureID), ...
                'WorkspaceVarNames', {'msrout'}, ...
                'FileVarNames', {measureID});
            
            clear msrout ranks
            
        end % for ties
        
        fprintf('  - elapsed time: %s\n', elapsedToHourMinutesSeconds(toc(start)));
        
    end % for measure
        
    fprintf('\n\n######## Total elapsed time for computing rank measures on track %s (%s): %s ########\n\n', ...
            EXPERIMENT.track.(trackID).name, EXPERIMENT.label.paper, elapsedToHourMinutesSeconds(toc(startComputation)));
end
