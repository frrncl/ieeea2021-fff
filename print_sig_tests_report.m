%% print_sig_tests_report
% 
% Reports a summary about the different statistical significance tests
% and saves them to a |.tex| file.
%
%% Synopsis
%
%   [] = print_sig_tests_report(varargin)
%  
% *Parameters*
%
% * *|varargin|* - the identifiers of the tracks for which the report has to
% be printed.
%
% *Returns*
%
% Nothing
%

%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2017a or higher
% * *Copyright:* (C) 2018-2021 <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>


%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

function [] = print_sig_tests_report(varargin)

    persistent ALL_TRACKID MEASURES
    
    if isempty(ALL_TRACKID)
        ALL_TRACKID = 'ALL';       
        
        %           p  rbp05  rbp03  rbp08  dcg  dcg10  rr  r  ap  ndcg ndcg10
        MEASURES = [8      3      2      4    5     10   7  9   1     6     11];        
    end

  % check the number of input parameters
    narginchk(1, inf);

    % load common parameters
    common_parameters
        
    for k = 1:length(varargin)
        
        trackID = varargin{k};
                
        % check that trackID is a non-empty string
        validateattributes(trackID, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'trackID');
        
        if iscell(trackID)
            % check that trackID is a cell array of strings with one element
            assert(iscellstr(trackID) && numel(trackID) == 1, ...
                'MATTERS:IllegalArgument', 'Expected trackID to be a cell array of strings containing just one string.');
        end
        
        % remove useless white spaces, if any, and ensure it is a char row
        trackID = char(strtrim(trackID));
        trackID = trackID(:).';
        
        % check that trackID assumes a valid value
        validatestring(trackID, ...
            EXPERIMENT.track.list, '', 'trackID');
        
        varargin{k} = trackID;
        
    end
       
    % start of overall computations
    startComputation = tic;
    
    fprintf('\n\n######## Reporting summary significance tests analyses (%s) ########\n\n', ...
        EXPERIMENT.label.paper);
    
    fprintf('+ Settings\n');
    fprintf('  - computed on %s\n\n', datestr(now, 'yyyy-mm-dd at HH:MM:SS'));
    fprintf('  - tracks: %s \n', join(string(varargin), ", "));
    
    
    fprintf('+ Printing the report\n');
    
    % for each type of tie breaking
    for tt = 1:EXPERIMENT.ties.number
        ties = EXPERIMENT.ties.list{tt};
        
        fprintf('+ Breaking ties with %s\n', ties);
                
        
        % the file where the report has to be written
        reportID = EXPERIMENT.pattern.identifier.report.sigcounts(ALL_TRACKID, ties);
        fid = fopen(EXPERIMENT.pattern.file.report(ALL_TRACKID, reportID), 'w');
        
        
        fprintf(fid, '\\documentclass[11pt]{article} \n\n');
        
        fprintf(fid, '\\usepackage{amsmath}\n');
        fprintf(fid, '\\usepackage{multirow}\n');
        fprintf(fid, '\\usepackage{longtable}\n');
        fprintf(fid, '\\usepackage{colortbl}\n');
        fprintf(fid, '\\usepackage{lscape}\n');
        fprintf(fid, '\\usepackage{pdflscape}\n');
        fprintf(fid, '\\usepackage{rotating}\n');
        fprintf(fid, '\\usepackage[a2paper,landscape]{geometry}\n\n');
        
        fprintf(fid, '\\usepackage{xcolor}\n');
        fprintf(fid, '\\definecolor{lightgrey}{RGB}{219, 219, 219}\n');
        fprintf(fid, '\\definecolor{verylightblue}{RGB}{204, 229, 255}\n');
        fprintf(fid, '\\definecolor{lightblue}{RGB}{124, 216, 255}\n');
        fprintf(fid, '\\definecolor{blue}{RGB}{32, 187, 253}\n');
        fprintf(fid, '\\definecolor{Gray}{gray}{0.75}\n');
        
        fprintf(fid, '\\begin{document}\n\n');
        
        
        fprintf(fid, '\\title{Report on Significance Tests Analyses \\\\ on %s}\n\n', ...
            strrep(join(string(varargin), ", "), '_', '\_'));
        
        fprintf(fid, '\\author{Nicola Ferro}\n\n');
        
        fprintf(fid, '\\maketitle\n\n');
        
        
        fprintf(fid, 'Settings:\n');
        fprintf(fid, '\\begin{itemize}\n');
        
         for k = 1:length(varargin)
            trackID = varargin{k};
            fprintf(fid, '\\item track: %s -- %s \n', strrep(trackID, '_', '\_'), EXPERIMENT.track.(trackID).name);
            fprintf(fid, '\\begin{itemize}\n');
            fprintf(fid, '\\item topics: %d \n', EXPERIMENT.track.(trackID).topics);
            fprintf(fid, '\\item runs: %d \n', EXPERIMENT.track.(trackID).runs);
            fprintf(fid, '\\item run lenght: %d \n', EXPERIMENT.track.(trackID).runLength);
            fprintf(fid, '\\end{itemize}\n');
         end
        
        
        fprintf(fid, '\\item analysed measures:\n');
        fprintf(fid, '\\begin{itemize}\n');
        for m = 1:length(MEASURES)
            fprintf(fid, '\\item %s: %s\n', ...
                strrep(EXPERIMENT.measure.getAcronym(m), '_', '\_'), EXPERIMENT.measure.getName(MEASURES(m)));
        end
        fprintf(fid, '\\end{itemize}\n');
        
        fprintf(fid, '\\end{itemize}\n');
        
        fprintf(fid, '\\newpage\n');
        
        
        fprintf(fid, '\\begin{table}[tbp] \n');
        
        fprintf(fid, '\\centering \n');
        % fprintf(fid, '\\vspace*{-12em} \n');
        
        
        fprintf(fid, '\\caption{Summary of significance tests analyses. Tie breaking approach: \\texttt{%s}. Gray lines are tests which do not require an interval scale.}\n', ...
            ties);
        
        fprintf(fid, '\\label{tab:smrysigcnts}\n');
        fprintf(fid, '\\small \n');
        %fprintf(fid, '\\hspace*{-4.5em} \n');
        fprintf(fid, '\\begin{tabular}{|l*{%d}{||r|r|r|r}|} \n', length(varargin));
        
        fprintf(fid, '\\hline \n');
        
        for i = 1:length(varargin)
            
            % take whatever measure
            mid = EXPERIMENT.measure.list{1};
            
            trackID = varargin{i};
            
            cntID = EXPERIMENT.pattern.identifier.sigcounts(mid, ties, trackID);
            
            serload2(EXPERIMENT.pattern.file.analysis(trackID, cntID), ...
                'WorkspaceVarNames', {'cnt'}, ...
                'FileVarNames', {cntID});
            
            if i < length(varargin)
                fprintf(fid, '& \\multicolumn{4}{c||}{\\textbf{\\texttt{%s} -- %d system pairs}} ', ...
                    strrep(trackID, '_', '\_'), cnt.pairs);
            else
                fprintf(fid, '& \\multicolumn{4}{c|}{\\textbf{\\texttt{%s} -- %d system pairs}} ', ...
                    strrep(trackID, '_', '\_'), cnt.pairs);
            end
        end
        
        fprintf(fid, '\\\\ \n');
        
        fprintf(fid, '\\cline{2-%d} \n', length(varargin)*4 + 1);
        
        
        for m = 1:length(MEASURES)
            
            for i = 1:length(varargin)
                if i == 1
                    fprintf(fid, '\\multicolumn{1}{|c||}{\\textbf{%s}} & \\multicolumn{1}{c|}{\\textbf{Sig}}  & \\multicolumn{1}{c|}{\\textbf{S2NS (\\%%)}} & \\multicolumn{1}{c|}{\\textbf{NS2S (\\%%)}} & \\multicolumn{1}{c||}{$\\mathbf{\\Delta\\%%}$} ', ...
                        strrep(EXPERIMENT.measure.getAcronym(MEASURES(m)), '_', '\_'));
                elseif i == length(varargin)
                    fprintf(fid, '  & \\multicolumn{1}{c|}{\\textbf{Sig}}  & \\multicolumn{1}{c|}{\\textbf{S2NS (\\%%)}} & \\multicolumn{1}{c|}{\\textbf{NS2S (\\%%)}} & \\multicolumn{1}{c|}{$\\mathbf{\\Delta\\%%}$} \\\\ \n');
                else
                    fprintf(fid, '  & \\multicolumn{1}{c|}{\\textbf{Sig}}  & \\multicolumn{1}{c|}{\\textbf{S2NS (\\%%)}} & \\multicolumn{1}{c|}{\\textbf{NS2S (\\%%)}} & \\multicolumn{1}{c||}{$\\mathbf{\\Delta\\%%}$}');
                end
            end
            
            fprintf(fid, '\\hline \n');
            
            mid = EXPERIMENT.measure.list{MEASURES(m)};
            
            fprintf(fid, '\\rowcolor{Gray}  \n');
            fprintf(fid, 'Sign Test ');
            for i = 1:length(varargin)
                
                trackID = varargin{i};
                
                cntID = EXPERIMENT.pattern.identifier.sigcounts(mid, ties, trackID);
                
                serload2(EXPERIMENT.pattern.file.analysis(trackID, cntID), ...
                    'WorkspaceVarNames', {'cnt'}, ...
                    'FileVarNames', {cntID});
                
                fprintf(fid, ' & %d & %d (%.2f\\%%) & %d (%.2f\\%%) & %.2f\\%% ', ...
                    cnt.sgn.msr.sig, cnt.sgn.sig2notsig, cnt.sgn.sig2notsig/cnt.sgn.msr.sig * 100, cnt.sgn.notsig2sig, cnt.sgn.notsig2sig/cnt.sgn.msr.sig * 100, (cnt.sgn.sig2notsig + cnt.sgn.notsig2sig) / cnt.sgn.msr.sig * 100 );
                
            end % for trackID
            fprintf(fid, ' \\\\ \n');
            fprintf(fid, '\\hline \n');
            
            
            fprintf(fid, '\\rowcolor{Gray}  \n');
            fprintf(fid, 'Wilcoxon Rank Sum Test ');
            for i = 1:length(varargin)
                
                trackID = varargin{i};
                
                cntID = EXPERIMENT.pattern.identifier.sigcounts(mid, ties, trackID);
                
                serload2(EXPERIMENT.pattern.file.analysis(trackID, cntID), ...
                    'WorkspaceVarNames', {'cnt'}, ...
                    'FileVarNames', {cntID});
                
                fprintf(fid, ' & %d & %d (%.2f\\%%) & %d (%.2f\\%%) & %.2f\\%% ', ...
                    cnt.rnksum.msr.sig, cnt.rnksum.sig2notsig, cnt.rnksum.sig2notsig/cnt.rnksum.msr.sig * 100, cnt.rnksum.notsig2sig, cnt.rnksum.notsig2sig/cnt.rnksum.msr.sig * 100, (cnt.rnksum.sig2notsig + cnt.rnksum.notsig2sig) / cnt.rnksum.msr.sig * 100 );
                
                
            end % for trackID
            fprintf(fid, ' \\\\ \n');
            fprintf(fid, '\\hline \n');
            
            
            fprintf(fid, 'Wilcoxon Signed Rank Test ');
            for i = 1:length(varargin)
                
                trackID = varargin{i};
                
                cntID = EXPERIMENT.pattern.identifier.sigcounts(mid, ties, trackID);
                
                serload2(EXPERIMENT.pattern.file.analysis(trackID, cntID), ...
                    'WorkspaceVarNames', {'cnt'}, ...
                    'FileVarNames', {cntID});
                
                
                fprintf(fid, ' & %d & %d (%.2f\\%%) & %d (%.2f\\%%) & %.2f\\%% ', ...
                    cnt.sgnrnk.msr.sig, cnt.sgnrnk.sig2notsig, cnt.sgnrnk.sig2notsig/cnt.sgnrnk.msr.sig * 100, cnt.sgnrnk.notsig2sig, cnt.sgnrnk.notsig2sig/cnt.sgnrnk.msr.sig * 100, (cnt.sgnrnk.sig2notsig + cnt.sgnrnk.notsig2sig) / cnt.sgnrnk.msr.sig * 100 );
                
                
            end % for trackID
            fprintf(fid, ' \\\\ \n');
            fprintf(fid, '\\hline \n');
            
            fprintf(fid, 'Student''s t Test ');
            for i = 1:length(varargin)
                
                trackID = varargin{i};
                
                cntID = EXPERIMENT.pattern.identifier.sigcounts(mid, ties, trackID);
                
                serload2(EXPERIMENT.pattern.file.analysis(trackID, cntID), ...
                    'WorkspaceVarNames', {'cnt'}, ...
                    'FileVarNames', {cntID});
                
                
                fprintf(fid, ' & %d & %d (%.2f\\%%) & %d (%.2f\\%%) & %.2f\\%% ', ...
                    cnt.t.msr.sig, cnt.t.sig2notsig, cnt.t.sig2notsig/cnt.t.msr.sig * 100, cnt.t.notsig2sig, cnt.t.notsig2sig/cnt.t.msr.sig * 100, (cnt.t.sig2notsig + cnt.t.notsig2sig) / cnt.t.msr.sig * 100 );
                
                
            end % for trackID
            fprintf(fid, ' \\\\ \n');
            fprintf(fid, '\\hline \n');
            
            
            fprintf(fid, 'One-way ANOVA ');
            for i = 1:length(varargin)
                
                trackID = varargin{i};
                
                cntID = EXPERIMENT.pattern.identifier.sigcounts(mid, ties, trackID);
                
                serload2(EXPERIMENT.pattern.file.analysis(trackID, cntID), ...
                    'WorkspaceVarNames', {'cnt'}, ...
                    'FileVarNames', {cntID});
                
                
                
                fprintf(fid, ' & %d & %d (%.2f\\%%) & %d (%.2f\\%%) & %.2f\\%% ', ...
                    cnt.anova1.msr.sig, cnt.anova1.sig2notsig, cnt.anova1.sig2notsig/cnt.anova1.msr.sig * 100, cnt.anova1.notsig2sig, cnt.anova1.notsig2sig/cnt.anova1.msr.sig * 100, (cnt.anova1.sig2notsig + cnt.anova1.notsig2sig) / cnt.anova1.msr.sig * 100);
                
            end % for trackID
            fprintf(fid, ' \\\\ \n');
            fprintf(fid, '\\hline \n');
            
            fprintf(fid, '\\rowcolor{Gray}  \n');
            fprintf(fid, 'Kruskal-Wallis Test ');
            for i = 1:length(varargin)
                
                trackID = varargin{i};
                
                cntID = EXPERIMENT.pattern.identifier.sigcounts(mid, ties, trackID);
                
                serload2(EXPERIMENT.pattern.file.analysis(trackID, cntID), ...
                    'WorkspaceVarNames', {'cnt'}, ...
                    'FileVarNames', {cntID});
                
                fprintf(fid, ' & %d & %d (%.2f\\%%) & %d (%.2f\\%%) & %.2f\\%% ', ...
                    cnt.kw.msr.sig, cnt.kw.sig2notsig, cnt.kw.sig2notsig/cnt.kw.msr.sig * 100, cnt.kw.notsig2sig, cnt.kw.notsig2sig/cnt.kw.msr.sig * 100, (cnt.kw.sig2notsig + cnt.kw.notsig2sig) / cnt.kw.msr.sig * 100 );
                
                
            end % for trackID
            fprintf(fid, ' \\\\ \n');
            fprintf(fid, '\\hline \n');
            
            
            fprintf(fid, 'Two-way ANOVA ');
            for i = 1:length(varargin)
                
                trackID = varargin{i};
                
                cntID = EXPERIMENT.pattern.identifier.sigcounts(mid, ties, trackID);
                
                serload2(EXPERIMENT.pattern.file.analysis(trackID, cntID), ...
                    'WorkspaceVarNames', {'cnt'}, ...
                    'FileVarNames', {cntID});
                
                
                
                fprintf(fid, ' & %d & %d (%.2f\\%%) & %d (%.2f\\%%) & %.2f\\%% ', ...
                    cnt.anova2.msr.sig, cnt.anova2.sig2notsig, cnt.anova2.sig2notsig/cnt.anova2.msr.sig * 100, cnt.anova2.notsig2sig, cnt.anova2.notsig2sig/cnt.anova2.msr.sig * 100, (cnt.anova2.sig2notsig + cnt.anova2.notsig2sig) / cnt.anova2.msr.sig * 100 );
                
            end % for trackID
            fprintf(fid, ' \\\\ \n');
            fprintf(fid, '\\hline \n');
            
            fprintf(fid, '\\rowcolor{Gray}  \n');
            fprintf(fid, 'Friedman Test ');
            for i = 1:length(varargin)
                
                trackID = varargin{i};
                
                cntID = EXPERIMENT.pattern.identifier.sigcounts(mid, ties, trackID);
                
                serload2(EXPERIMENT.pattern.file.analysis(trackID, cntID), ...
                    'WorkspaceVarNames', {'cnt'}, ...
                    'FileVarNames', {cntID});
                
                
                fprintf(fid, ' & %d & %d (%.2f\\%%) & %d (%.2f\\%%) & %.2f\\%% ', ...
                    cnt.f.msr.sig, cnt.f.sig2notsig, cnt.f.sig2notsig/cnt.f.msr.sig * 100, cnt.f.notsig2sig, cnt.f.notsig2sig/cnt.f.msr.sig * 100, (cnt.f.sig2notsig + cnt.f.notsig2sig) / cnt.f.msr.sig * 100 );
                
                
            end % for trackID
            fprintf(fid, ' \\\\ \n');
            
            fprintf(fid, '\\hline \\hline \n');
            
            
            
        end % for measure
        
        fprintf(fid, '\\end{tabular} \n');
        
        fprintf(fid, '\\end{table} \n\n');
        
        
        fprintf(fid, '\\end{document} \n\n');
        
        fclose(fid);
        
    end % for ties
                       
    fprintf('\n\n######## Total elapsed time for reporting summary significance tests analyses (%s): %s ########\n\n', ...
            EXPERIMENT.label.paper, elapsedToHourMinutesSeconds(toc(startComputation)));

        
end
