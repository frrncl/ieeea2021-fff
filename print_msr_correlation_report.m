%% print_msr_correlation_report
% 
% Reports a summary about the overall correlation among measures, both
% between the original measures and between their ranked version, and saves 
% them to a|.tex| file.
%
%% Synopsis
%
%   [] = print_correlation_report(varargin)
%  
% *Parameters*
%
% if you wish remove ties and assign ties the rank in the unique list.
% * *|varargin|* - the identifiers of the tracks for which the report has to
% be printed.
%
% *Returns*
%
% Nothing
%

%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2017a or higher
% * *Copyright:* (C) 2018-2021 <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>


%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

function [] = print_msr_correlation_report(varargin)

   persistent MEASURES ALL_TRACKID
    
      if isempty(MEASURES)
        MEASURES = [ 8  3; ... p vs rbp05
                     8  7; ... p vs rr
                     8  2; ... p vs rbp03
                     8  4; ... p vs rbp08
                     8  5; ... p vs dcg
                     8 10; ... p vs dcg10
                     3  2; ... rbp05 vs rbp03
                     3  4; ... rbp05 vs rbp08
                     3  7; ... rbp05 vs rr
                     3  5; ... rbp05 vs dcg
                     3 10; ... rbp05 vs dcg10
                     2  4; ... rbp03 vs rbp08
                     2  7; ... rbp03 vs rr
                     2  5; ... rbp03 vs dcg
                     2 10; ... rbp03 vs dcg10
                     4  7; ... rbp08 vs rr
                     4  5; ... rbp08 vs dcg
                     4 10; ... rbp08 vs dcg10
                     7  5; ... rr vs dcg
                     7 10; ... rr vs dcg10
                     5 10; ... dcg vs dcg10
                     9  8; ... r vs p
                     9  3; ... r vs rbp05
                     9  2; ... r vs rbp03
                     9  4; ... r vs rbp08
                     9  7; ... r vs rr
                     9  5; ... r vs dcg
                     9 10; ... r vs dcg10
                     9  1; ... r vs ap
                     9  6; ... r vs ndcg
                     9 11; ... r vs ndcg10
                     1  8; ... ap vs p
                     1  3; ... ap vs rbp05
                     1  2; ... ap vs rbp03
                     1  4; ... ap vs rbp08
                     1  7; ... ap vs rr
                     1  5; ... ap vs dcg
                     1 10; ... ap vs dcg10
                     1  6; ... ap vs ndcg
                     1 11; ... ap vs ndcg10
                     6  8; ... ndcg vs p
                     6  3; ... ndcg vs rbp05
                     6  2; ... ndcg vs rbp03
                     6  4; ... ndcg vs rbp08
                     6  7; ... ndcg vs rr
                     6  5; ... ndcg vs dcg
                     6 10; ... ndcg vs dcg10
                     6 11; ... ndcg vs ndcg10
                    11  8; ... ndcg10 vs p
                    11  3; ... ndcg10 vs rbp05
                    11  2; ... ndcg10 vs rbp03
                    11  4; ... ndcg10 vs rbp08
                    11  7; ... ndcg10 vs rr
                    11  5; ... ndcg10 vs dcg
                    11 10; ... ndcg10 vs dcg10
                   ];             
                  
         ALL_TRACKID = 'ALL';  
    end

    
    % check the number of input parameters
    narginchk(1, inf);

    % load common parameters
    common_parameters
        
    for k = 1:length(varargin)
        
        trackID = varargin{k};
                
        % check that trackID is a non-empty string
        validateattributes(trackID, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'trackID');
        
        if iscell(trackID)
            % check that trackID is a cell array of strings with one element
            assert(iscellstr(trackID) && numel(trackID) == 1, ...
                'MATTERS:IllegalArgument', 'Expected trackID to be a cell array of strings containing just one string.');
        end
        
        % remove useless white spaces, if any, and ensure it is a char row
        trackID = char(strtrim(trackID));
        trackID = trackID(:).';
        
        % check that trackID assumes a valid value
        validatestring(trackID, ...
            EXPERIMENT.track.list, '', 'trackID');
        
        varargin{k} = trackID;
        
    end
       
    % start of overall computations
    startComputation = tic;
    
    fprintf('\n\n######## Reporting summary correlation analyses (%s) ########\n\n', ...
        EXPERIMENT.label.paper);

    fprintf('+ Settings\n');
    fprintf('  - computed on %s\n', datestr(now, 'yyyy-mm-dd at HH:MM:SS'));    
    fprintf('  - tracks: %s \n', join(string(varargin), ", "));

        
    fprintf('+ Printing the report\n');    
    
    % for each type of tie breaking
    for tt = 1:EXPERIMENT.ties.number
        ties = EXPERIMENT.ties.list{tt};
        
        fprintf('+ Breaking ties with %s\n', ties);
        
        
        % the file where the report has to be written
        reportID = EXPERIMENT.pattern.identifier.report.correlation(ALL_TRACKID, 'msr_ovr', ties);
        fid = fopen(EXPERIMENT.pattern.file.report(ALL_TRACKID, reportID), 'w');
        
        
        fprintf(fid, '\\documentclass[11pt]{article} \n\n');
        
        fprintf(fid, '\\usepackage{amsmath}\n');
        fprintf(fid, '\\usepackage{multirow}\n');
        fprintf(fid, '\\usepackage{longtable}\n');
        fprintf(fid, '\\usepackage{colortbl}\n');
        fprintf(fid, '\\usepackage{lscape}\n');
        fprintf(fid, '\\usepackage{pdflscape}\n');
        fprintf(fid, '\\usepackage{rotating}\n');
        fprintf(fid, '\\usepackage[a3paper,landscape]{geometry}\n\n');
        
        fprintf(fid, '\\usepackage{xcolor}\n');
        fprintf(fid, '\\definecolor{lightgrey}{RGB}{219, 219, 219}\n');
        fprintf(fid, '\\definecolor{verylightblue}{RGB}{204, 229, 255}\n');
        fprintf(fid, '\\definecolor{lightblue}{RGB}{124, 216, 255}\n');
        fprintf(fid, '\\definecolor{blue}{RGB}{32, 187, 253}\n');
        
        fprintf(fid, '\\begin{document}\n\n');
        
        
        fprintf(fid, '\\title{Report on Correlation Analysis \\\\ on %s}\n\n', ...
            strrep(join(string(varargin), ", "), '_', '\_'));
        
        fprintf(fid, '\\author{Nicola Ferro}\n\n');
        
        fprintf(fid, '\\maketitle\n\n');
        
        
        fprintf(fid, 'Settings:\n');
        fprintf(fid, '\\begin{itemize}\n');
        
        for k = 1:length(varargin)
            trackID = varargin{k};
            fprintf(fid, '\\item track: %s -- %s \n', strrep(trackID, '_', '\_'), EXPERIMENT.track.(trackID).name);
            fprintf(fid, '\\begin{itemize}\n');
            fprintf(fid, '\\item topics: %d \n', EXPERIMENT.track.(trackID).topics);
            fprintf(fid, '\\item runs: %d \n', EXPERIMENT.track.(trackID).runs);
            fprintf(fid, '\\item run lenght: %d \n', EXPERIMENT.track.(trackID).runLength);
            fprintf(fid, '\\end{itemize}\n');
        end
        
        fprintf(fid, '\\item tie breaking approach: \\texttt{%s}\n', ties);
                       
        fprintf(fid, '\\end{itemize}\n');
        
        fprintf(fid, '\\newpage\n');
        
        fprintf(fid, '\\begin{table}[tb] \n');
        
        fprintf(fid, '\\centering \n');
        % fprintf(fid, '\\vspace*{-12em} \n');
        
        
        fprintf(fid, '\\caption{Summary Kendall''s $\\tau$ correlation analysis on tracks %s.}\n', ...
            strrep(join(string(varargin), ", "), '_', '\_'));
        
        fprintf(fid, '\\label{tab:smrytau}\n');
        %fprintf(fid, '\\tiny \n');
        %fprintf(fid, '\\hspace*{-8.5em} \n');
        fprintf(fid, '\\begin{tabular}{|l*{%d}{||r|r|r}|} \n', length(varargin));
        
        fprintf(fid, '\\hline\\hline \n');
               
        fprintf(fid, '\\multicolumn{1}{|c||}{\\textbf{Measure}} ');
        
        for m = 1:length(varargin)
            if m < length(varargin)
                fprintf(fid, '& \\multicolumn{3}{c||}{\\textbf{\\texttt{%s}}} ', ...
                    strrep(varargin{m}, '_', '\_'));
            else
                fprintf(fid, '& \\multicolumn{3}{c|}{\\textbf{\\texttt{%s}}} ', ...
                    strrep(varargin{m}, '_', '\_'));
            end
        end
        
        fprintf(fid, '\\\\ \n');
                        
        fprintf(fid, '\\cline{2-%d} \n', length(varargin)*3 + 1);
        
        for m = 1:length(varargin)
            
            if m < length(varargin)
                fprintf(fid, '& \\multicolumn{1}{c|}{\\textbf{Msr}} & \\multicolumn{1}{c|}{\\textbf{RnkMsr}} & \\multicolumn{1}{c||}{$\\mathbf{\\Delta\\%%}$} ');
            else
                fprintf(fid, '& \\multicolumn{1}{c|}{\\textbf{Msr}} & \\multicolumn{1}{c|}{\\textbf{RnkMsr}} & \\multicolumn{1}{c|}{$\\mathbf{\\Delta\\%%}$} ');
            end
        end
        
        fprintf(fid, '\\\\ \n');
        
        fprintf(fid, '\\hline \n');
        
        
        for m = 1:length(MEASURES)
            
            fprintf(fid, '%s vs %s ', strrep(EXPERIMENT.measure.getAcronym(MEASURES(m, 1)), '_', '\_'), strrep(EXPERIMENT.measure.getAcronym(MEASURES(m, 2)), '_', '\_'));
            
            for k = 1:length(varargin)
                
                trackID = varargin{k};
                
                tauID = EXPERIMENT.pattern.identifier.tau.msr(trackID, ties);
                
                serload2(EXPERIMENT.pattern.file.analysis(trackID, tauID), ...
                    'WorkspaceVarNames', {'tau'}, ...
                    'FileVarNames', {tauID});
                
                fprintf(fid, ' & %5.4f & %5.4f & %+5.2f\\%% ', ...
                    tau.values.ordinary(m), tau.values.ranked(m), (tau.values.ranked(m) - tau.values.ordinary(m))/tau.values.ordinary(m) * 100 );
                
            end
            
            fprintf(fid, '  \\\\ \n');
            
            fprintf(fid, '\\hline \n');
            
            
        end
        
        fprintf(fid, '\\hline \n');
        
        fprintf(fid, '\\end{tabular} \n');
        
        fprintf(fid, '\\end{table} \n\n');
        
        fprintf(fid, '\\end{document} \n\n');
        
        fclose(fid);
        
    end % for ties
    
    fprintf('\n\n######## Total elapsed time for reporting summary correlation analyses (%s): %s ########\n\n', ...
            EXPERIMENT.label.paper, elapsedToHourMinutesSeconds(toc(startComputation)));

        
end
