%% print_rnk_ovr_correlation_report
% 
% Reports a summary about the overall correlation between a
% measure and its respectice ranked version and saves them to a
% |.tex| file.
%
%% Synopsis
%
%   [] = print_rnk_ovr_correlation_report(varargin)
%  
% *Parameters*
%
% * *|varargin|* - the identifiers of the tracks for which the report has to
% be printed.
%
% *Returns*
%
% Nothing
%

%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2017a or higher
% * *Copyright:* (C) 2018-2021 <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>


%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

function [] = print_rnk_ovr_correlation_report(varargin)

    persistent ALL_TRACKID MEASURES
    
    if isempty(ALL_TRACKID)
        ALL_TRACKID = 'ALL';       
        
        %           p  rbp05  rr  rbp03  rbp08  dcg  dcg10  r  ap  ndcg ndcg10
        MEASURES = [8      3   7      2      4    5     10  9   1     6     11];        
    end
    
    % check the number of input parameters
    narginchk(1, inf);

    % load common parameters
    common_parameters
        
    for k = 1:length(varargin)
        
        trackID = varargin{k};
                
        % check that trackID is a non-empty string
        validateattributes(trackID, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'trackID');
        
        if iscell(trackID)
            % check that trackID is a cell array of strings with one element
            assert(iscellstr(trackID) && numel(trackID) == 1, ...
                'MATTERS:IllegalArgument', 'Expected trackID to be a cell array of strings containing just one string.');
        end
        
        % remove useless white spaces, if any, and ensure it is a char row
        trackID = char(strtrim(trackID));
        trackID = trackID(:).';
        
        % check that trackID assumes a valid value
        validatestring(trackID, ...
            EXPERIMENT.track.list, '', 'trackID');
        
        varargin{k} = trackID;
        
    end
       
    % start of overall computations
    startComputation = tic;
    
    fprintf('\n\n######## Reporting summary correlation analyses (%s) ########\n\n', ...
        EXPERIMENT.label.paper);

    fprintf('+ Settings\n');
    fprintf('  - computed on %s\n', datestr(now, 'yyyy-mm-dd at HH:MM:SS'));    
    fprintf('  - tracks: %s \n', join(string(varargin), ", "));

        
    fprintf('+ Printing the report\n');    
    
    % for each type of tie breaking
    for tt = 1:EXPERIMENT.ties.number
        ties = EXPERIMENT.ties.list{tt};
        
        fprintf('+ Breaking ties with %s\n', ties);
        
        
        % the file where the report has to be written
        reportID = EXPERIMENT.pattern.identifier.report.correlation(ALL_TRACKID, 'rnk_ovr', ties);
        fid = fopen(EXPERIMENT.pattern.file.report(ALL_TRACKID, reportID), 'w');
        
        
        fprintf(fid, '\\documentclass[11pt]{article} \n\n');
        
        fprintf(fid, '\\usepackage{amsmath}\n');
        fprintf(fid, '\\usepackage{multirow}\n');
        fprintf(fid, '\\usepackage{longtable}\n');
        fprintf(fid, '\\usepackage{colortbl}\n');
        fprintf(fid, '\\usepackage{lscape}\n');
        fprintf(fid, '\\usepackage{pdflscape}\n');
        fprintf(fid, '\\usepackage{rotating}\n');
        fprintf(fid, '\\usepackage[a3paper]{geometry}\n\n');
        
        fprintf(fid, '\\usepackage{xcolor}\n');
        fprintf(fid, '\\definecolor{lightgrey}{RGB}{219, 219, 219}\n');
        fprintf(fid, '\\definecolor{verylightblue}{RGB}{204, 229, 255}\n');
        fprintf(fid, '\\definecolor{lightblue}{RGB}{124, 216, 255}\n');
        fprintf(fid, '\\definecolor{blue}{RGB}{32, 187, 253}\n');
        
        fprintf(fid, '\\begin{document}\n\n');
        
        
        fprintf(fid, '\\title{Report on Overall Correlation Analysis between Measures and their Ranked Version \\\\ on %s}\n\n', ...
            strrep(join(string(varargin), ", "), '_', '\_'));
        
        fprintf(fid, '\\author{Nicola Ferro}\n\n');
        
        fprintf(fid, '\\maketitle\n\n');
        
        
        fprintf(fid, 'Settings:\n');
        fprintf(fid, '\\begin{itemize}\n');
        
        for k = 1:length(varargin)
            trackID = varargin{k};
            fprintf(fid, '\\item track: %s -- %s \n', strrep(trackID, '_', '\_'), EXPERIMENT.track.(trackID).name);
            fprintf(fid, '\\begin{itemize}\n');
            fprintf(fid, '\\item topics: %d \n', EXPERIMENT.track.(trackID).topics);
            fprintf(fid, '\\item runs: %d \n', EXPERIMENT.track.(trackID).runs);
            fprintf(fid, '\\item run lenght: %d \n', EXPERIMENT.track.(trackID).runLength);
            fprintf(fid, '\\end{itemize}\n');
        end
        
        fprintf(fid, '\\item tie breaking approach: \\texttt{%s}\n', ties);
        
        
        fprintf(fid, '\\item analysed measures:\n');
        fprintf(fid, '\\begin{itemize}\n');
        for m = 1:length(MEASURES)
            fprintf(fid, '\\item %s: %s\n', ...
                strrep(EXPERIMENT.measure.getAcronym(m), '_', '\_'), EXPERIMENT.measure.getName(MEASURES(m)));
        end
        fprintf(fid, '\\end{itemize}\n');
        
        fprintf(fid, '\\end{itemize}\n');
        
        fprintf(fid, '\\newpage\n');
        
        fprintf(fid, '\\begin{table}[tb] \n');
        
        fprintf(fid, '\\centering \n');
        % fprintf(fid, '\\vspace*{-12em} \n');
        
        
        fprintf(fid, '\\caption{Summary Kendall''s $\\tau$ overall correlation analysis between measures and their ranked version on tracks %s.}\n', ...
            strrep(join(string(varargin), ", "), '_', '\_'));
        
        fprintf(fid, '\\label{tab:smrytau}\n');
        %fprintf(fid, '\\tiny \n');
        %fprintf(fid, '\\hspace*{-8.5em} \n');
        fprintf(fid, '\\begin{tabular}{|l|*{%d}{r|}} \n', length(MEASURES));
        
        fprintf(fid, '\\hline\\hline \n');
        
        fprintf(fid, '\\multicolumn{%d}{|c|}{\\textbf{Tie breaking approach: \\texttt{%s}.}} \\\\ \n', ...
            length(MEASURES) + 1, ties);
        
        fprintf(fid, '\\multicolumn{1}{|c|}{\\textbf{Track}} ');
        
        for m = 1:length(MEASURES)
            fprintf(fid, '& \\multicolumn{1}{c|}{\\textbf{%s}} ', ...
                strrep(EXPERIMENT.measure.getAcronym(MEASURES(m)), '_', '\_'));
        end
        
        fprintf(fid, '\\\\ \n');
        
        fprintf(fid, '\\hline \n');
                       
        for k = 1:length(varargin)
            
            trackID = varargin{k};
            
            tauID = EXPERIMENT.pattern.identifier.tau.rnk(trackID, ties);
            
            serload2(EXPERIMENT.pattern.file.analysis(trackID, tauID), ...
                'WorkspaceVarNames', {'tau'}, ...
                'FileVarNames', {tauID});
            
            fprintf(fid, '\\texttt{%s} ', strrep(trackID, '_', '\_'));
            
            for m = 1:length(MEASURES)
                
                fprintf(fid, ' & %5.4f ', ...
                    tau.overall.values(MEASURES(m)));
            end
            
            fprintf(fid, '  \\\\ \n');
            
            fprintf(fid, '\\hline \n');
            
            
        end
                
        fprintf(fid, '\\hline \n');
        
        fprintf(fid, '\\end{tabular} \n');
        
        fprintf(fid, '\\end{table} \n\n');
        
        fprintf(fid, '\\end{document} \n\n');
        
        fclose(fid);
        
    end % for ties
    
    fprintf('\n\n######## Total elapsed time for reporting summary correlation analyses (%s): %s ########\n\n', ...
            EXPERIMENT.label.paper, elapsedToHourMinutesSeconds(toc(startComputation)));

        
end
