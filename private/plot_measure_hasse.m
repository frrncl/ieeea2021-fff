%% plot_measure_hasse
% 
% Plots the Hasse diagram for the given runs and measure.

%% Synopsis
%
%   [h] = plot_measure_hasse(runs, m)
%  
% *Parameters*
%
% * *|runs|* - the input run as a matrix of |0| (not relevant) and |1|
% relevant; each row is a different run; the left-most column is the
% top-ranked document, the right-most column is the bottom-ranked document.
% * *|m|* - the measure computed on the set of runs. Each element is the
% score of the measure for the corresponding row of |runs|.
% * *|label|* - the label for the Y-axis.
%
%
% *Returns*
%
% * |h|  - an handle to the figure.
%
%
%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2017a or higher
% * *Copyright:* (C) 2018-2021 <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>
%
%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

%%
function [h] = plot_measure_hasse(runs, m, label)

    narginchk(3, 3);
    
    % sorts the measure in ascending order
    [m, idx] = sort(m);

    % sort the runs as the measure
    runs =  runs(idx, :);
    
    % the handle to the current figure
    h = figure('Visible', 'off');

    y = -0.05:0.01:max(m)*1.05;

    % draw a vertical black line
    plot(zeros(size(y)), y, 'Color', 'black', 'LineWidth', 1.5);
    hold on
   
    % draw squares corresponding to the values of the measure
    plot(zeros(size(m)), m, 's', 'MarkerSize', 20, 'MarkerEdgeColor', 'blue', 'MarkerFaceColor', 'blue');
    
    % write the run corresponding to each square, dealing with ties
    oldValue = NaN;  
    maxIndentCount = 1;    
    for i  = 1:length(m)       

        % if we are dealing with a tied runs, increase its indentation and
        % the counter of the tied runs for that value
        if (m(i) == oldValue)
            indent = indent + 0.4;
            indentCount = indentCount + 1;
        else 
        % otherwise reset indentation and the counter of tied runs
            indent = 0.07;
            indentCount = 1;
        end        
        
        t = text(indent, m(i), sprintf('(%d, %d, %d, %d)', runs(i,:)));
        t.FontSize= 24;
        
        oldValue = m(i);
        maxIndentCount = max(maxIndentCount, indentCount);
    end
   
    ax = gca;
   
    ax.FontSize = 24;

    ax.XTick = [];
    ax.XTickLabel = [];
        
    ax.YTick = unique(m);
    ytickformat('%.3f') 
      
    ax.XLim = [0 0.5 * maxIndentCount];
    ax.YLim = [y(1) y(end)];
   
    ylabel(label)
    
    h.PaperPositionMode = 'auto';
    h.PaperUnits = 'centimeters';
    
    % tweak maxIndexCount to better adjust paper size
    if (maxIndentCount == 1)
        maxIndentCount = 1.5;
    elseif (maxIndentCount >= 6)
        maxIndentCount = maxIndentCount * 0.9;
    end
            
    h.PaperSize = [ (8 * maxIndentCount + 2) 52];
    h.PaperPosition = [1 1 (8 * maxIndentCount) 50];
          
end