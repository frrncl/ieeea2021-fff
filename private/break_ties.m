%% break_ties
% 
% Breaks ties according to the requested strategy.
%
% Code is derived from Mathworks unique.m and tiedrank.m.
%
%% Synopsis
%
%   [rnk] = break_ties(data, mode) 
%  
%
% Note that, for breaking ties, data is sorted in ascending order so the
% assigned ranks refer to such ascending order.
%
% *Parameters*
%
% * *|data|* - the data whose rank has to be computed
% * *|mode|* - the way of breaking ties. |mid| if you wish to use midranks,
% i.e. the same as calling |tiedrank|; |min| if you wish to use the minimum
% of tied ranks; |max| if you wish to use the maximum of tied ranks; |unq|
% if you wish remove ties and assign ties the rank in the unique list.
%
% *Returns*
%
% * *|rnk|* - the ranking of the data.
%
% Nothing
%

%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2017a or higher
% * *Copyright:* (C) 2018-2021 <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

function [rnk] = break_ties(data, mode)   

    % not very nice way of parsing but it makes follow-up computations
    % a little bit faster     
    doMin = false;
    doMid = false;
    doUnq = false;

    switch mode
        case 'min'
            doMin = true;
            
        case 'mid'
            doMid = true;
                        
        case 'unq'
            doUnq = true;
    end

    % sort the data
    [s, idx] = sort(data(:));

    % find where two adjacent positions are equal, i.e. ties, or not
    d = diff(s);

    if doUnq

        % the location of the not tied elements
        nottied = [true; d ~= 0];

        % Lists the positions of each element starting from 1.
        % Tied elements get the same position
        rnk = cumsum(nottied);

        % Re-reference rnk to indexing of s.
        rnk(idx) = rnk;

    else

        % the ranks
        ranks = 1:length(data);

        % find the positions of the tied values
        % length(data) + 2 just ensures a stop condition
        tieloc = [find(d==0); length(data)+2];

        maxTies = numel(tieloc);

        tiecount = 1;
        while (tiecount < maxTies)

            % pick up the current tied value
            tiestart = tieloc(tiecount);
            ntied = 2;

            % count how many ties there are for that value
            while(tieloc(tiecount+1) == tieloc(tiecount)+1)
                tiecount = tiecount+1;
                ntied = ntied+1;
            end
            
            if doMid
                % Compute mean of tied ranks
                ranks(tiestart:tiestart+ntied-1) = ...
                    sum(ranks(tiestart:tiestart+ntied-1)) / ntied;
            else                
                if doMin
                    % Compute minimum of tied ranks
                    ranks(tiestart:tiestart+ntied-1) = ranks(tiestart);
                else
                    % Compute maximum of tied ranks
                    ranks(tiestart:tiestart+ntied-1) = ranks(tiestart+ntied-1);
                end                
            end

            tiecount = tiecount + 1;
        end

        rnk(idx) = ranks;
    end

    rnk = reshape(rnk, size(data));
  
end
