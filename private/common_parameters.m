%% common_parameters
% 
% Sets up parameters common to the different scripts.
%
%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2017a or higher
% * *Copyright:* (C) 2018-2021 <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

diary off;

%% Path Configuration

hn = hostname();

if (strcmpi(hn, 'grace.dei.unipd.it'))    
    addpath(genpath('/ssd/data/ferro/matters/')) 
    EXPERIMENT.path.base = '/ssd/data/ferro/IEEEA2021-FFF/experiment/';
elseif (strcmpi(hn, 'hpcapri'))     
    addpath(genpath('/stor/progetti/p1022/p1022u00/matters/')) 
    EXPERIMENT.path.base = '/stor/progetti/p1022/p1022u00/IEEEA2021-FFF/experiment/';
else % if we are running on the local machine
    EXPERIMENT.path.base = '/Users/ferro/Documents/pubblicazioni/2021/IEEEA-FFF/experiment/';  
end


% The path for the datasets, i.e. the runs and the pools of both original
% tracks and sub-corpora
EXPERIMENT.path.dataset = sprintf('%1$s%2$s%3$s', EXPERIMENT.path.base, 'dataset', filesep);

% The path for the measures
EXPERIMENT.path.measure = sprintf('%1$s%2$s%3$s', EXPERIMENT.path.base, 'measure', filesep);

% The path for analyses
EXPERIMENT.path.analysis = sprintf('%1$s%2$s%3$s', EXPERIMENT.path.base, 'analysis', filesep);

% The path for figures
EXPERIMENT.path.figure = sprintf('%1$s%2$s%3$s', EXPERIMENT.path.base, 'figure', filesep);

% The path for reports
EXPERIMENT.path.report = sprintf('%1$s%2$s%3$s', EXPERIMENT.path.base, 'report', filesep);

%% General Configuration

% Label of the paper this experiment is for
EXPERIMENT.label.paper = 'IEEE Access 2021 FFF';

%% Configuration for Tracks

EXPERIMENT.track.list = {'ACR', ...
                         'T08',     'T08_05', 'T08_10', 'T08_20', 'T08_30', ...
                         'T26_10k', 'T26_05', 'T26_10', 'T26_20', 'T26_30', ...
                         'T27_10k', 'T27_05', 'T27_10', 'T27_20', 'T27_30'};
EXPERIMENT.track.number = length(EXPERIMENT.track.list);

% ACR
EXPERIMENT.track.ACR.id = 'ACR';
EXPERIMENT.track.ACR.name = 'All Cases Runs - All the possible binary runs of a given length';

% TREC 08, 1999, Adhoc
EXPERIMENT.track.T08.id = 'T08';
EXPERIMENT.track.T08.name = 'TREC 08, 1999, Adhoc - Official Length Runs';
EXPERIMENT.track.T08.runs = 129;
EXPERIMENT.track.T08.topics = 50;
EXPERIMENT.track.T08.runLength = 1000;
EXPERIMENT.track.T08.pool.file = '/Users/ferro/Documents/experimental-collections/TREC/TREC_08_1999_AdHoc/pool/qrels.trec8.adhoc.txt';
EXPERIMENT.track.T08.pool.relevanceDegrees = {'NotRelevant', 'Relevant'};
EXPERIMENT.track.T08.pool.relevanceGrades = 0:1;
EXPERIMENT.track.T08.pool.delimiter = 'space';
EXPERIMENT.track.T08.pool.import = @(fileName, id) importPoolFromFileTRECFormat('FileName', fileName, 'Identifier', id, 'RelevanceDegrees', EXPERIMENT.track.T08.pool.relevanceDegrees, 'RelevanceGrades', EXPERIMENT.track.T08.pool.relevanceGrades, 'Delimiter', EXPERIMENT.track.T08.pool.delimiter,  'Verbose', false);
EXPERIMENT.track.T08.run.path = '/Users/ferro/Documents/experimental-collections/TREC/TREC_08_1999_AdHoc/runs/all';
EXPERIMENT.track.T08.run.documentOrdering = 'TrecEvalLexDesc';
EXPERIMENT.track.T08.run.singlePrecision = true;
EXPERIMENT.track.T08.run.delimiter = 'tab';
EXPERIMENT.track.T08.run.import = @(runPath, id, requiredTopics) importRunsFromDirectoryTRECFormat('Path', runPath, 'Identifier', id, 'RequiredTopics', requiredTopics, 'DocumentOrdering', EXPERIMENT.track.T08.run.documentOrdering, 'SinglePrecision', EXPERIMENT.track.T08.run.singlePrecision, 'Delimiter', EXPERIMENT.track.T08.run.delimiter, 'Verbose', false);

% TREC 08, 1999, Adhoc - Runs at length 5
EXPERIMENT.track.T08_05.id = 'T08_05';
EXPERIMENT.track.T08_05.name = 'TREC 08, 1999, Adhoc - Runs at length 5';
EXPERIMENT.track.T08_05.runs = 129;
EXPERIMENT.track.T08_05.topics = 50;
EXPERIMENT.track.T08_05.runLength = 5;
EXPERIMENT.track.T08_05.pool.file = '/Users/ferro/Documents/experimental-collections/TREC/TREC_08_1999_AdHoc/pool/qrels.trec8.adhoc.txt';
EXPERIMENT.track.T08_05.pool.relevanceDegrees = {'NotRelevant', 'Relevant'};
EXPERIMENT.track.T08_05.pool.relevanceGrades = 0:1;
EXPERIMENT.track.T08_05.pool.delimiter = 'space';
EXPERIMENT.track.T08_05.pool.import = @(fileName, id) importPoolFromFileTRECFormat('FileName', fileName, 'Identifier', id, 'RelevanceDegrees', EXPERIMENT.track.T08_05.pool.relevanceDegrees, 'RelevanceGrades', EXPERIMENT.track.T08_05.pool.relevanceGrades, 'Delimiter', EXPERIMENT.track.T08_05.pool.delimiter,  'Verbose', false);
EXPERIMENT.track.T08_05.run.path = [];
EXPERIMENT.track.T08_05.run.documentOrdering = 'TrecEvalLexDesc';
EXPERIMENT.track.T08_05.run.singlePrecision = true;
EXPERIMENT.track.T08_05.run.delimiter = 'tab';
EXPERIMENT.track.T08_05.run.import = @(runPath, id, requiredTopics) importRunsFromDirectoryTRECFormat('Path', runPath, 'Identifier', id, 'RequiredTopics', requiredTopics, 'DocumentOrdering', EXPERIMENT.track.T08_05.run.documentOrdering, 'SinglePrecision', EXPERIMENT.track.T08_05.run.singlePrecision, 'Delimiter', EXPERIMENT.track.T08_05.run.delimiter, 'Verbose', false);

% TREC 08, 1999, Adhoc - Runs at length 10
EXPERIMENT.track.T08_10.id = 'T08_10';
EXPERIMENT.track.T08_10.name = 'TREC 08, 1999, Adhoc - Runs at length 10';
EXPERIMENT.track.T08_10.runs = 129;
EXPERIMENT.track.T08_10.topics = 50;
EXPERIMENT.track.T08_10.runLength = 10;
EXPERIMENT.track.T08_10.pool.file = '/Users/ferro/Documents/experimental-collections/TREC/TREC_08_1999_AdHoc/pool/qrels.trec8.adhoc.txt';
EXPERIMENT.track.T08_10.pool.relevanceDegrees = {'NotRelevant', 'Relevant'};
EXPERIMENT.track.T08_10.pool.relevanceGrades = 0:1;
EXPERIMENT.track.T08_10.pool.delimiter = 'space';
EXPERIMENT.track.T08_10.pool.import = @(fileName, id) importPoolFromFileTRECFormat('FileName', fileName, 'Identifier', id, 'RelevanceDegrees', EXPERIMENT.track.T08_10.pool.relevanceDegrees, 'RelevanceGrades', EXPERIMENT.track.T08_10.pool.relevanceGrades, 'Delimiter', EXPERIMENT.track.T08_10.pool.delimiter,  'Verbose', false);
EXPERIMENT.track.T08_10.run.path = [];
EXPERIMENT.track.T08_10.run.documentOrdering = 'TrecEvalLexDesc';
EXPERIMENT.track.T08_10.run.singlePrecision = true;
EXPERIMENT.track.T08_10.run.delimiter = 'tab';
EXPERIMENT.track.T08_10.run.import = @(runPath, id, requiredTopics) importRunsFromDirectoryTRECFormat('Path', runPath, 'Identifier', id, 'RequiredTopics', requiredTopics, 'DocumentOrdering', EXPERIMENT.track.T08_10.run.documentOrdering, 'SinglePrecision', EXPERIMENT.track.T08_10.run.singlePrecision, 'Delimiter', EXPERIMENT.track.T08_10.run.delimiter, 'Verbose', false);

% TREC 08, 1999, Adhoc - Runs at length 20
EXPERIMENT.track.T08_20.id = 'T08_20';
EXPERIMENT.track.T08_20.name = 'TREC 08, 1999, Adhoc - Runs at length 20';
EXPERIMENT.track.T08_20.runs = 129;
EXPERIMENT.track.T08_20.topics = 50;
EXPERIMENT.track.T08_20.runLength = 20;
EXPERIMENT.track.T08_20.pool.file = '/Users/ferro/Documents/experimental-collections/TREC/TREC_08_1999_AdHoc/pool/qrels.trec8.adhoc.txt';
EXPERIMENT.track.T08_20.pool.relevanceDegrees = {'NotRelevant', 'Relevant'};
EXPERIMENT.track.T08_20.pool.relevanceGrades = 0:1;
EXPERIMENT.track.T08_20.pool.delimiter = 'space';
EXPERIMENT.track.T08_20.pool.import = @(fileName, id) importPoolFromFileTRECFormat('FileName', fileName, 'Identifier', id, 'RelevanceDegrees', EXPERIMENT.track.T08_20.pool.relevanceDegrees, 'RelevanceGrades', EXPERIMENT.track.T08_20.pool.relevanceGrades, 'Delimiter', EXPERIMENT.track.T08_20.pool.delimiter,  'Verbose', false);
EXPERIMENT.track.T08_20.run.path = [];
EXPERIMENT.track.T08_20.run.documentOrdering = 'TrecEvalLexDesc';
EXPERIMENT.track.T08_20.run.singlePrecision = true;
EXPERIMENT.track.T08_20.run.delimiter = 'tab';
EXPERIMENT.track.T08_20.run.import = @(runPath, id, requiredTopics) importRunsFromDirectoryTRECFormat('Path', runPath, 'Identifier', id, 'RequiredTopics', requiredTopics, 'DocumentOrdering', EXPERIMENT.track.T08_20.run.documentOrdering, 'SinglePrecision', EXPERIMENT.track.T08_20.run.singlePrecision, 'Delimiter', EXPERIMENT.track.T08_20.run.delimiter, 'Verbose', false);

% TREC 08, 1999, Adhoc - Runs at length 30
EXPERIMENT.track.T08_30.id = 'T08_30';
EXPERIMENT.track.T08_30.name = 'TREC 08, 1999, Adhoc - Runs at length 30';
EXPERIMENT.track.T08_30.runs = 129;
EXPERIMENT.track.T08_30.topics = 50;
EXPERIMENT.track.T08_30.runLength = 30;
EXPERIMENT.track.T08_30.pool.file = '/Users/ferro/Documents/experimental-collections/TREC/TREC_08_1999_AdHoc/pool/qrels.trec8.adhoc.txt';
EXPERIMENT.track.T08_30.pool.relevanceDegrees = {'NotRelevant', 'Relevant'};
EXPERIMENT.track.T08_30.pool.relevanceGrades = 0:1;
EXPERIMENT.track.T08_30.pool.delimiter = 'space';
EXPERIMENT.track.T08_30.pool.import = @(fileName, id) importPoolFromFileTRECFormat('FileName', fileName, 'Identifier', id, 'RelevanceDegrees', EXPERIMENT.track.T08_30.pool.relevanceDegrees, 'RelevanceGrades', EXPERIMENT.track.T08_30.pool.relevanceGrades, 'Delimiter', EXPERIMENT.track.T08_30.pool.delimiter,  'Verbose', false);
EXPERIMENT.track.T08_30.run.path = [];
EXPERIMENT.track.T08_30.run.documentOrdering = 'TrecEvalLexDesc';
EXPERIMENT.track.T08_30.run.singlePrecision = true;
EXPERIMENT.track.T08_30.run.delimiter = 'tab';
EXPERIMENT.track.T08_30.run.import = @(runPath, id, requiredTopics) importRunsFromDirectoryTRECFormat('Path', runPath, 'Identifier', id, 'RequiredTopics', requiredTopics, 'DocumentOrdering', EXPERIMENT.track.T08_30.run.documentOrdering, 'SinglePrecision', EXPERIMENT.track.T08_30.run.singlePrecision, 'Delimiter', EXPERIMENT.track.T08_30.run.delimiter, 'Verbose', false);

% Track TREC 26, 2017, Core - Official Length Runs
EXPERIMENT.track.T26_10k.id = 'T26_10k';
EXPERIMENT.track.T26_10k.name =  'TREC 26, 2017, Core - Official Length Runs';
EXPERIMENT.track.T26_10k.runs = 75;
EXPERIMENT.track.T26_10k.topics = 50;
EXPERIMENT.track.T26_10k.runLength = 10000;
EXPERIMENT.track.T26_10k.pool.file = '/Users/ferro/Documents/experimental-collections/TREC/TREC_26_2017_Core/pool/qrels.trec26.core.bin.txt';
EXPERIMENT.track.T26_10k.pool.relevanceDegrees = {'NotRelevant', 'Relevant'};
EXPERIMENT.track.T26_10k.pool.relevanceGrades = 0:1;
EXPERIMENT.track.T26_10k.pool.delimiter = 'space';
EXPERIMENT.track.T26_10k.pool.import = @(fileName, id) importPoolFromFileTRECFormat('FileName', fileName, 'Identifier', id, 'RelevanceDegrees', EXPERIMENT.track.T26_10k.pool.relevanceDegrees, 'RelevanceGrades', EXPERIMENT.track.T26_10k.pool.relevanceGrades, 'Delimiter', EXPERIMENT.track.T26_10k.pool.delimiter,  'Verbose', false);
EXPERIMENT.track.T26_10k.run.path = '/Users/ferro/Documents/experimental-collections/TREC/TREC_26_2017_Core/runs/all';
EXPERIMENT.track.T26_10k.run.documentOrdering = 'TrecEvalLexDesc';
EXPERIMENT.track.T26_10k.run.singlePrecision = true;
EXPERIMENT.track.T26_10k.run.delimiter = 'tab';
EXPERIMENT.track.T26_10k.run.import = @(runPath, id, requiredTopics) importRunsFromDirectoryTRECFormat('Path', runPath, 'Identifier', id, 'RequiredTopics', requiredTopics, 'DocumentOrdering', EXPERIMENT.track.T26_10k.run.documentOrdering, 'SinglePrecision', EXPERIMENT.track.T26_10k.run.singlePrecision, 'Delimiter', EXPERIMENT.track.T26_10k.run.delimiter, 'Verbose', false);

% Track TREC 26, 2017, Core - Runs at length 5
EXPERIMENT.track.T26_05.id = 'T26_05';
EXPERIMENT.track.T26_05.name =  'TREC 26, 2017, Core - Runs at length 5';
EXPERIMENT.track.T26_05.runs = 75;
EXPERIMENT.track.T26_05.topics = 50;
EXPERIMENT.track.T26_05.runLength = 5;
EXPERIMENT.track.T26_05.pool.file = '/Users/ferro/Documents/experimental-collections/TREC/TREC_26_2017_Core/pool/qrels.trec26.core.bin.txt';
EXPERIMENT.track.T26_05.pool.relevanceDegrees = {'NotRelevant', 'Relevant'};
EXPERIMENT.track.T26_05.pool.relevanceGrades = 0:1;
EXPERIMENT.track.T26_05.pool.delimiter = 'space';
EXPERIMENT.track.T26_05.pool.import = @(fileName, id) importPoolFromFileTRECFormat('FileName', fileName, 'Identifier', id, 'RelevanceDegrees', EXPERIMENT.track.T26_05.pool.relevanceDegrees, 'RelevanceGrades', EXPERIMENT.track.T26_05.pool.relevanceGrades, 'Delimiter', EXPERIMENT.track.T26_05.pool.delimiter,  'Verbose', false);
EXPERIMENT.track.T26_05.run.path = [];
EXPERIMENT.track.T26_05.run.documentOrdering = 'TrecEvalLexDesc';
EXPERIMENT.track.T26_05.run.singlePrecision = true;
EXPERIMENT.track.T26_05.run.delimiter = 'tab';
EXPERIMENT.track.T26_05.run.import = @(runPath, id, requiredTopics) importRunsFromDirectoryTRECFormat('Path', runPath, 'Identifier', id, 'RequiredTopics', requiredTopics, 'DocumentOrdering', EXPERIMENT.track.T26_05.run.documentOrdering, 'SinglePrecision', EXPERIMENT.track.T26_05.run.singlePrecision, 'Delimiter', EXPERIMENT.track.T26_05.run.delimiter, 'Verbose', false);

% Track TREC 26, 2017, Core - Runs at length 10
EXPERIMENT.track.T26_10.id = 'T26_10';
EXPERIMENT.track.T26_10.name =  'TREC 26, 2017, Core - Runs at length 10';
EXPERIMENT.track.T26_10.runs = 75;
EXPERIMENT.track.T26_10.topics = 50;
EXPERIMENT.track.T26_10.runLength = 10;
EXPERIMENT.track.T26_10.pool.file = '/Users/ferro/Documents/experimental-collections/TREC/TREC_26_2017_Core/pool/qrels.trec26.core.bin.txt';
EXPERIMENT.track.T26_10.pool.relevanceDegrees = {'NotRelevant', 'Relevant'};
EXPERIMENT.track.T26_10.pool.relevanceGrades = 0:1;
EXPERIMENT.track.T26_10.pool.delimiter = 'space';
EXPERIMENT.track.T26_10.pool.import = @(fileName, id) importPoolFromFileTRECFormat('FileName', fileName, 'Identifier', id, 'RelevanceDegrees', EXPERIMENT.track.T26_10.pool.relevanceDegrees, 'RelevanceGrades', EXPERIMENT.track.T26_10.pool.relevanceGrades, 'Delimiter', EXPERIMENT.track.T26_10.pool.delimiter,  'Verbose', false);
EXPERIMENT.track.T26_10.run.path = [];
EXPERIMENT.track.T26_10.run.documentOrdering = 'TrecEvalLexDesc';
EXPERIMENT.track.T26_10.run.singlePrecision = true;
EXPERIMENT.track.T26_10.run.delimiter = 'tab';
EXPERIMENT.track.T26_10.run.import = @(runPath, id, requiredTopics) importRunsFromDirectoryTRECFormat('Path', runPath, 'Identifier', id, 'RequiredTopics', requiredTopics, 'DocumentOrdering', EXPERIMENT.track.T26_10.run.documentOrdering, 'SinglePrecision', EXPERIMENT.track.T26_10.run.singlePrecision, 'Delimiter', EXPERIMENT.track.T26_10.run.delimiter, 'Verbose', false);

% Track TREC 26, 2017, Core - Runs at length 20
EXPERIMENT.track.T26_20.id = 'T26_20';
EXPERIMENT.track.T26_20.name =  'TREC 26, 2017, Core - Runs at length 20';
EXPERIMENT.track.T26_20.runs = 75;
EXPERIMENT.track.T26_20.topics = 50;
EXPERIMENT.track.T26_20.runLength = 20;
EXPERIMENT.track.T26_20.pool.file = '/Users/ferro/Documents/experimental-collections/TREC/TREC_26_2017_Core/pool/qrels.trec26.core.bin.txt';
EXPERIMENT.track.T26_20.pool.relevanceDegrees = {'NotRelevant', 'Relevant'};
EXPERIMENT.track.T26_20.pool.relevanceGrades = 0:1;
EXPERIMENT.track.T26_20.pool.delimiter = 'space';
EXPERIMENT.track.T26_20.pool.import = @(fileName, id) importPoolFromFileTRECFormat('FileName', fileName, 'Identifier', id, 'RelevanceDegrees', EXPERIMENT.track.T26_20.pool.relevanceDegrees, 'RelevanceGrades', EXPERIMENT.track.T26_20.pool.relevanceGrades, 'Delimiter', EXPERIMENT.track.T26_20.pool.delimiter,  'Verbose', false);
EXPERIMENT.track.T26_20.run.path = [];
EXPERIMENT.track.T26_20.run.documentOrdering = 'TrecEvalLexDesc';
EXPERIMENT.track.T26_20.run.singlePrecision = true;
EXPERIMENT.track.T26_20.run.delimiter = 'tab';
EXPERIMENT.track.T26_20.run.import = @(runPath, id, requiredTopics) importRunsFromDirectoryTRECFormat('Path', runPath, 'Identifier', id, 'RequiredTopics', requiredTopics, 'DocumentOrdering', EXPERIMENT.track.T26_20.run.documentOrdering, 'SinglePrecision', EXPERIMENT.track.T26_20.run.singlePrecision, 'Delimiter', EXPERIMENT.track.T26_20.run.delimiter, 'Verbose', false);

% Track TREC 26, 2017, Core - Runs at length 30
EXPERIMENT.track.T26_30.id = 'T26_30';
EXPERIMENT.track.T26_30.name =  'TREC 26, 2017, Core - Runs at length 30';
EXPERIMENT.track.T26_30.runs = 75;
EXPERIMENT.track.T26_30.topics = 50;
EXPERIMENT.track.T26_30.runLength = 30;
EXPERIMENT.track.T26_30.pool.file = '/Users/ferro/Documents/experimental-collections/TREC/TREC_26_2017_Core/pool/qrels.trec26.core.bin.txt';
EXPERIMENT.track.T26_30.pool.relevanceDegrees = {'NotRelevant', 'Relevant'};
EXPERIMENT.track.T26_30.pool.relevanceGrades = 0:1;
EXPERIMENT.track.T26_30.pool.delimiter = 'space';
EXPERIMENT.track.T26_30.pool.import = @(fileName, id) importPoolFromFileTRECFormat('FileName', fileName, 'Identifier', id, 'RelevanceDegrees', EXPERIMENT.track.T26_30.pool.relevanceDegrees, 'RelevanceGrades', EXPERIMENT.track.T26_30.pool.relevanceGrades, 'Delimiter', EXPERIMENT.track.T26_30.pool.delimiter,  'Verbose', false);
EXPERIMENT.track.T26_30.run.path = [];
EXPERIMENT.track.T26_30.run.documentOrdering = 'TrecEvalLexDesc';
EXPERIMENT.track.T26_30.run.singlePrecision = true;
EXPERIMENT.track.T26_30.run.delimiter = 'tab';
EXPERIMENT.track.T26_30.run.import = @(runPath, id, requiredTopics) importRunsFromDirectoryTRECFormat('Path', runPath, 'Identifier', id, 'RequiredTopics', requiredTopics, 'DocumentOrdering', EXPERIMENT.track.T26_30.run.documentOrdering, 'SinglePrecision', EXPERIMENT.track.T26_30.run.singlePrecision, 'Delimiter', EXPERIMENT.track.T26_30.run.delimiter, 'Verbose', false);

% Track TREC 27, 2018, Core - Official Length Runs
EXPERIMENT.track.T27_10k.id = 'T27_10k';
EXPERIMENT.track.T27_10k.name =  'TREC 27, 2018, Core - Official Length Runs';
EXPERIMENT.track.T27_10k.runs = 72;
EXPERIMENT.track.T27_10k.topics = 50;
EXPERIMENT.track.T27_10k.runLength = 10000;
EXPERIMENT.track.T27_10k.pool.file = '/Users/ferro/Documents/experimental-collections/TREC/TREC_27_2018_Core/pool/qrels.trec27.core.bin.txt';
EXPERIMENT.track.T27_10k.pool.relevanceDegrees = {'NotRelevant', 'Relevant'};
EXPERIMENT.track.T27_10k.pool.relevanceGrades = 0:1;
EXPERIMENT.track.T27_10k.pool.delimiter = 'space';
EXPERIMENT.track.T27_10k.pool.import = @(fileName, id) importPoolFromFileTRECFormat('FileName', fileName, 'Identifier', id, 'RelevanceDegrees', EXPERIMENT.track.T27_10k.pool.relevanceDegrees, 'RelevanceGrades', EXPERIMENT.track.T27_10k.pool.relevanceGrades, 'Delimiter', EXPERIMENT.track.T27_10k.pool.delimiter,  'Verbose', false);
EXPERIMENT.track.T27_10k.run.path = '/Users/ferro/Documents/experimental-collections/TREC/TREC_27_2018_Core/runs';
EXPERIMENT.track.T27_10k.run.documentOrdering = 'TrecEvalLexDesc';
EXPERIMENT.track.T27_10k.run.singlePrecision = true;
EXPERIMENT.track.T27_10k.run.delimiter = 'tab';
EXPERIMENT.track.T27_10k.run.import = @(runPath, id, requiredTopics) importRunsFromDirectoryTRECFormat('Path', runPath, 'Identifier', id, 'RequiredTopics', requiredTopics, 'DocumentOrdering', EXPERIMENT.track.T27_10k.run.documentOrdering, 'SinglePrecision', EXPERIMENT.track.T27_10k.run.singlePrecision, 'Delimiter', EXPERIMENT.track.T27_10k.run.delimiter, 'Verbose', false);

% Track TREC 27, 2018, Core - Runs at length 5
EXPERIMENT.track.T27_05.id = 'T27_05';
EXPERIMENT.track.T27_05.name =  'TREC 27, 2018, Core - Runs at length 5';
EXPERIMENT.track.T27_05.runs = 72;
EXPERIMENT.track.T27_05.topics = 50;
EXPERIMENT.track.T27_05.runLength = 5;
EXPERIMENT.track.T27_05.pool.file = '/Users/ferro/Documents/experimental-collections/TREC/TREC_27_2018_Core/pool/qrels.trec27.core.bin.txt';
EXPERIMENT.track.T27_05.pool.relevanceDegrees = {'NotRelevant', 'Relevant'};
EXPERIMENT.track.T27_05.pool.relevanceGrades = 0:1;
EXPERIMENT.track.T27_05.pool.delimiter = 'space';
EXPERIMENT.track.T27_05.pool.import = @(fileName, id) importPoolFromFileTRECFormat('FileName', fileName, 'Identifier', id, 'RelevanceDegrees', EXPERIMENT.track.T27_05.pool.relevanceDegrees, 'RelevanceGrades', EXPERIMENT.track.T27_05.pool.relevanceGrades, 'Delimiter', EXPERIMENT.track.T27_05.pool.delimiter,  'Verbose', false);
EXPERIMENT.track.T27_05.run.path = [];
EXPERIMENT.track.T27_05.run.documentOrdering = 'TrecEvalLexDesc';
EXPERIMENT.track.T27_05.run.singlePrecision = true;
EXPERIMENT.track.T27_05.run.delimiter = 'tab';
EXPERIMENT.track.T27_05.run.import = @(runPath, id, requiredTopics) importRunsFromDirectoryTRECFormat('Path', runPath, 'Identifier', id, 'RequiredTopics', requiredTopics, 'DocumentOrdering', EXPERIMENT.track.T27_05.run.documentOrdering, 'SinglePrecision', EXPERIMENT.track.T27_05.run.singlePrecision, 'Delimiter', EXPERIMENT.track.T27_05.run.delimiter, 'Verbose', false);

% Track TREC 27, 2018, Core - Runs at length 10
EXPERIMENT.track.T27_10.id = 'T27_10';
EXPERIMENT.track.T27_10.name =  'TREC 27, 2018, Core - Runs at length 10';
EXPERIMENT.track.T27_10.runs = 72;
EXPERIMENT.track.T27_10.topics = 50;
EXPERIMENT.track.T27_10.runLength = 10;
EXPERIMENT.track.T27_10.pool.file = '/Users/ferro/Documents/experimental-collections/TREC/TREC_27_2018_Core/pool/qrels.trec27.core.bin.txt';
EXPERIMENT.track.T27_10.pool.relevanceDegrees = {'NotRelevant', 'Relevant'};
EXPERIMENT.track.T27_10.pool.relevanceGrades = 0:1;
EXPERIMENT.track.T27_10.pool.delimiter = 'space';
EXPERIMENT.track.T27_10.pool.import = @(fileName, id) importPoolFromFileTRECFormat('FileName', fileName, 'Identifier', id, 'RelevanceDegrees', EXPERIMENT.track.T27_10.pool.relevanceDegrees, 'RelevanceGrades', EXPERIMENT.track.T27_10.pool.relevanceGrades, 'Delimiter', EXPERIMENT.track.T27_10.pool.delimiter,  'Verbose', false);
EXPERIMENT.track.T27_10.run.path = [];
EXPERIMENT.track.T27_10.run.documentOrdering = 'TrecEvalLexDesc';
EXPERIMENT.track.T27_10.run.singlePrecision = true;
EXPERIMENT.track.T27_10.run.delimiter = 'tab';
EXPERIMENT.track.T27_10.run.import = @(runPath, id, requiredTopics) importRunsFromDirectoryTRECFormat('Path', runPath, 'Identifier', id, 'RequiredTopics', requiredTopics, 'DocumentOrdering', EXPERIMENT.track.T27_10.run.documentOrdering, 'SinglePrecision', EXPERIMENT.track.T27_10.run.singlePrecision, 'Delimiter', EXPERIMENT.track.T27_10.run.delimiter, 'Verbose', false);

% Track TREC 27, 2018, Core - Runs at length 20
EXPERIMENT.track.T27_20.id = 'T27_20';
EXPERIMENT.track.T27_20.name =  'TREC 27, 2018, Core - Runs at length 20';
EXPERIMENT.track.T27_20.runs = 72;
EXPERIMENT.track.T27_20.topics = 50;
EXPERIMENT.track.T27_20.runLength = 20;
EXPERIMENT.track.T27_20.pool.file = '/Users/ferro/Documents/experimental-collections/TREC/TREC_27_2018_Core/pool/qrels.trec27.core.bin.txt';
EXPERIMENT.track.T27_20.pool.relevanceDegrees = {'NotRelevant', 'Relevant'};
EXPERIMENT.track.T27_20.pool.relevanceGrades = 0:1;
EXPERIMENT.track.T27_20.pool.delimiter = 'space';
EXPERIMENT.track.T27_20.pool.import = @(fileName, id) importPoolFromFileTRECFormat('FileName', fileName, 'Identifier', id, 'RelevanceDegrees', EXPERIMENT.track.T27_20.pool.relevanceDegrees, 'RelevanceGrades', EXPERIMENT.track.T27_20.pool.relevanceGrades, 'Delimiter', EXPERIMENT.track.T27_20.pool.delimiter,  'Verbose', false);
EXPERIMENT.track.T27_20.run.path = [];
EXPERIMENT.track.T27_20.run.documentOrdering = 'TrecEvalLexDesc';
EXPERIMENT.track.T27_20.run.singlePrecision = true;
EXPERIMENT.track.T27_20.run.delimiter = 'tab';
EXPERIMENT.track.T27_20.run.import = @(runPath, id, requiredTopics) importRunsFromDirectoryTRECFormat('Path', runPath, 'Identifier', id, 'RequiredTopics', requiredTopics, 'DocumentOrdering', EXPERIMENT.track.T27_20.run.documentOrdering, 'SinglePrecision', EXPERIMENT.track.T27_20.run.singlePrecision, 'Delimiter', EXPERIMENT.track.T27_20.run.delimiter, 'Verbose', false);

% Track TREC 27, 2018, Core - Runs at length 30
EXPERIMENT.track.T27_30.id = 'T27_30';
EXPERIMENT.track.T27_30.name =  'TREC 27, 2018, Core - Runs at length 30';
EXPERIMENT.track.T27_30.runs = 72;
EXPERIMENT.track.T27_30.topics = 50;
EXPERIMENT.track.T27_30.runLength = 30;
EXPERIMENT.track.T27_30.pool.file = '/Users/ferro/Documents/experimental-collections/TREC/TREC_27_2018_Core/pool/qrels.trec27.core.bin.txt';
EXPERIMENT.track.T27_30.pool.relevanceDegrees = {'NotRelevant', 'Relevant'};
EXPERIMENT.track.T27_30.pool.relevanceGrades = 0:1;
EXPERIMENT.track.T27_30.pool.delimiter = 'space';
EXPERIMENT.track.T27_30.pool.import = @(fileName, id) importPoolFromFileTRECFormat('FileName', fileName, 'Identifier', id, 'RelevanceDegrees', EXPERIMENT.track.T27_30.pool.relevanceDegrees, 'RelevanceGrades', EXPERIMENT.track.T27_30.pool.relevanceGrades, 'Delimiter', EXPERIMENT.track.T27_30.pool.delimiter,  'Verbose', false);
EXPERIMENT.track.T27_30.run.path = [];
EXPERIMENT.track.T27_30.run.documentOrdering = 'TrecEvalLexDesc';
EXPERIMENT.track.T27_30.run.singlePrecision = true;
EXPERIMENT.track.T27_30.run.delimiter = 'tab';
EXPERIMENT.track.T27_30.run.import = @(runPath, id, requiredTopics) importRunsFromDirectoryTRECFormat('Path', runPath, 'Identifier', id, 'RequiredTopics', requiredTopics, 'DocumentOrdering', EXPERIMENT.track.T27_30.run.documentOrdering, 'SinglePrecision', EXPERIMENT.track.T27_30.run.singlePrecision, 'Delimiter', EXPERIMENT.track.T27_30.run.delimiter, 'Verbose', false);

% Returns the name of a track given its index in EXPERIMENT.track.list
EXPERIMENT.track.getName = @(idx) ( EXPERIMENT.track.(EXPERIMENT.track.list{idx}).name ); 

%% Patterns for file names

% MAT - Pattern <path>/<trackID>/<fileID>.<ext> 
EXPERIMENT.pattern.file.general = @(path, trackID, fileID, ext) sprintf('%1$s%2$s%3$s%4$s.%5$s', path, trackID, filesep, fileID, ext);
 
% MAT - Pattern EXPERIMENT.path.base/dataset/<trackID>/<datasetID>.mat
EXPERIMENT.pattern.file.dataset = @(trackID, datasetID) EXPERIMENT.pattern.file.general(EXPERIMENT.path.dataset, trackID, datasetID, 'mat');

% MAT - Pattern EXPERIMENT.path.base/measure/<trackID>/<measureID>.mat
EXPERIMENT.pattern.file.measure = @(trackID, measureID) EXPERIMENT.pattern.file.general(EXPERIMENT.path.measure, trackID, measureID, 'mat');

% MAT - Pattern EXPERIMENT.path.base/analysis/<trackID>/<analysisID>.mat
EXPERIMENT.pattern.file.analysis = @(trackID, analysisID) EXPERIMENT.pattern.file.general(EXPERIMENT.path.analysis, trackID, analysisID, 'mat');

% PDF - Pattern EXPERIMENT.path.base/figure/<trackID>/<figureID>
EXPERIMENT.pattern.file.figure = @(trackID, figureID) EXPERIMENT.pattern.file.general(EXPERIMENT.path.figure, trackID, figureID, 'pdf');

% TEX - Pattern EXPERIMENT.path.base/report/<trackID>/<reportID>
EXPERIMENT.pattern.file.report = @(trackID, reportID) EXPERIMENT.pattern.file.general(EXPERIMENT.path.report, trackID, reportID, 'tex');


%% Patterns for identifiers

% Pattern pool_<trackID>
EXPERIMENT.pattern.identifier.pool =  @(trackID) sprintf('pool_%1$s', trackID);

% Pattern run_<trackID>
EXPERIMENT.pattern.identifier.run = @(trackID) sprintf('run_%1$s', trackID);

% Pattern <measureID>_<trackID>
EXPERIMENT.pattern.identifier.measure =  @(measureID, trackID) sprintf('%1$s_%2$s', measureID, trackID);

% Pattern rnk_<measureID>_<ties>_<trackID>
EXPERIMENT.pattern.identifier.rankMeasure =  @(measureID, ties, trackID) sprintf('rnk_%1$s_%2$s_%3$s', measureID, ties, trackID);

% Pattern acr_<measureID>_N<N>
EXPERIMENT.pattern.identifier.acr.measure =  @(measureID, N) sprintf('acr_%1$s_N%2$04d', measureID, N);

% Pattern acrrnk_<measureID>_<ties>_N<N>
EXPERIMENT.pattern.identifier.acr.ranks =  @(measureID, ties, N) sprintf('acrrnk_%1$s_%2$s_N%3$04d', measureID, ties, N);

% Pattern tau_rnk_<ties>_<trackID>
EXPERIMENT.pattern.identifier.tau.rnk = @(trackID, ties) sprintf('tau_rnk_%1$s_%2$s', ties, trackID);

% Pattern tau_msr_<ties>_<trackID>
EXPERIMENT.pattern.identifier.tau.msr = @(trackID, ties) sprintf('tau_msr_%1$s_%2$s', ties, trackID);

% Pattern sigcnt_<measureID>_<ties>_<trackID>
EXPERIMENT.pattern.identifier.sigcounts =  @(measureID, ties, trackID) sprintf('sigcnt_%1$s_%2$s_%3$s', measureID, ties, trackID);

% Pattern tau_<type>_<ties>_<trackID>
EXPERIMENT.pattern.identifier.report.correlation =  @(trackID, type, ties) sprintf('tau_%2$s_%3$s_%1$s', trackID, type, ties);

% Pattern sigcnt_report_<ties>_<trackID>
EXPERIMENT.pattern.identifier.report.sigcounts =  @(trackID, ties) sprintf('sigcnt_report_%2$s_%1$s', trackID, ties);


%% Configuration for measures

% The list of helper measures
EXPERIMENT.measure.helper = {'idx', 'rb'};
EXPERIMENT.measure.helperNumber = length(EXPERIMENT.measure.helper);

% The list of measures under experimentation
EXPERIMENT.measure.list = [{'ap', 'rbp03', 'rbp05', 'rbp08', 'dcg', 'ndcg', 'rr', 'p', 'r', 'dcg10', 'ndcg10', 'cg'} ...
                           EXPERIMENT.measure.helper];
EXPERIMENT.measure.number = length(EXPERIMENT.measure.list);

% Configuration for AP
EXPERIMENT.measure.ap.id = 'ap';
EXPERIMENT.measure.ap.acronym = 'AP';
EXPERIMENT.measure.ap.name = 'Average Precision';
EXPERIMENT.measure.ap.compute = @(pool, runSet, runLength) averagePrecision(pool, runSet);
EXPERIMENT.measure.ap.myCompute = @(runs) fast_ap(runs);

% Configuration for RBP with p = 0.3
EXPERIMENT.measure.rbp03.id = 'rbp03';
EXPERIMENT.measure.rbp03.acronym = 'RBP_p03';
EXPERIMENT.measure.rbp03.name = 'Rank-biased Precision';
EXPERIMENT.measure.rbp03.persistence = 0.3;
EXPERIMENT.measure.rbp03.compute = @(pool, runSet, runLength) rankBiasedPrecision(pool, runSet, 'Persistence', EXPERIMENT.measure.rbp03.persistence);
EXPERIMENT.measure.rbp03.myCompute = @(runs) fast_rbp(runs, EXPERIMENT.measure.rbp03.persistence);

% Configuration for RBP with p = 0.5
EXPERIMENT.measure.rbp05.id = 'rbp05';
EXPERIMENT.measure.rbp05.acronym = 'RBP_p05';
EXPERIMENT.measure.rbp05.name = 'Rank-biased Precision';
EXPERIMENT.measure.rbp05.persistence = 0.5;
EXPERIMENT.measure.rbp05.compute = @(pool, runSet, runLength) rankBiasedPrecision(pool, runSet, 'Persistence', EXPERIMENT.measure.rbp05.persistence);
EXPERIMENT.measure.rbp05.myCompute = @(runs) fast_rbp(runs, EXPERIMENT.measure.rbp05.persistence);

% Configuration for RBP with p = 0.8
EXPERIMENT.measure.rbp08.id = 'rbp08';
EXPERIMENT.measure.rbp08.acronym = 'RBP_p08';
EXPERIMENT.measure.rbp08.name = 'Rank-biased Precision';
EXPERIMENT.measure.rbp08.persistence = 0.8;
EXPERIMENT.measure.rbp08.compute = @(pool, runSet, runLength) rankBiasedPrecision(pool, runSet, 'Persistence', EXPERIMENT.measure.rbp08.persistence);
EXPERIMENT.measure.rbp08.myCompute = @(runs) fast_rbp(runs, EXPERIMENT.measure.rbp08.persistence);

% Configuration for DCG with log base 2
EXPERIMENT.measure.dcg.id = 'dcg';
EXPERIMENT.measure.dcg.acronym = 'DCG_b02';
EXPERIMENT.measure.dcg.name = 'Discounted Cumulated Gain';
EXPERIMENT.measure.dcg.logBase = 2;
EXPERIMENT.measure.dcg.fixedNumberRetrievedDocumentsPaddingStrategy = 'NotRelevant';
EXPERIMENT.measure.dcg.compute = @(pool, runSet, runLength) discountedCumulatedGain(pool, runSet, 'CutOffs', runLength, 'LogBase', EXPERIMENT.measure.dcg.logBase, 'Normalize', false, 'FixNumberRetrievedDocuments', runLength, 'FixedNumberRetrievedDocumentsPaddingStrategy', EXPERIMENT.measure.dcg.fixedNumberRetrievedDocumentsPaddingStrategy);
EXPERIMENT.measure.dcg.myCompute = @(runs) fast_dcg(runs, EXPERIMENT.measure.dcg.logBase);

% Configuration for DCG with log base 2
EXPERIMENT.measure.ndcg.id = 'ndcg';
EXPERIMENT.measure.ndcg.acronym = 'nDCG_b02';
EXPERIMENT.measure.ndcg.name = 'Normalized Discounted Cumulated Gain';
EXPERIMENT.measure.ndcg.logBase = 2;
EXPERIMENT.measure.ndcg.fixedNumberRetrievedDocumentsPaddingStrategy = 'NotRelevant';
EXPERIMENT.measure.ndcg.compute = @(pool, runSet, runLength) discountedCumulatedGain(pool, runSet, 'CutOffs', runLength, 'LogBase', EXPERIMENT.measure.ndcg.logBase, 'Normalize', true, 'FixNumberRetrievedDocuments', runLength, 'FixedNumberRetrievedDocumentsPaddingStrategy', EXPERIMENT.measure.ndcg.fixedNumberRetrievedDocumentsPaddingStrategy);
EXPERIMENT.measure.ndcg.myCompute = @(runs) fast_dcg(runs, EXPERIMENT.measure.ndcg.logBase);

% Configuration for RR
EXPERIMENT.measure.rr.id = 'rr';
EXPERIMENT.measure.rr.acronym = 'RR';
EXPERIMENT.measure.rr.name = 'Reciprocal Rank';
EXPERIMENT.measure.rr.compute = @(pool, runSet, runLength) reciprocalRank(pool, runSet);
EXPERIMENT.measure.rr.myCompute = @(runs) fast_rr(runs);

% Configuration for P
EXPERIMENT.measure.p.id = 'p';
EXPERIMENT.measure.p.acronym = 'P';
EXPERIMENT.measure.p.name = 'Precision';
EXPERIMENT.measure.p.compute = @(pool, runSet, runLength) precision(pool, runSet, 'Cutoffs', runLength);
EXPERIMENT.measure.p.myCompute = @(runs) fast_p(runs);

% Configuration for R
EXPERIMENT.measure.r.id = 'r';
EXPERIMENT.measure.r.acronym = 'R';
EXPERIMENT.measure.r.name = 'Recall';
EXPERIMENT.measure.r.compute = @(pool, runSet, runLength) recall(pool, runSet);
EXPERIMENT.measure.r.myCompute = @(runs) fast_r(runs);

% Configuration for DCG with log base 10
EXPERIMENT.measure.dcg10.id = 'dcg10';
EXPERIMENT.measure.dcg10.acronym = 'DCG_b10';
EXPERIMENT.measure.dcg10.name = 'Discounted Cumulated Gain';
EXPERIMENT.measure.dcg10.logBase = 10;
EXPERIMENT.measure.dcg10.fixedNumberRetrievedDocumentsPaddingStrategy = 'NotRelevant';
EXPERIMENT.measure.dcg10.compute = @(pool, runSet, runLength) discountedCumulatedGain(pool, runSet, 'CutOffs', runLength, 'LogBase', EXPERIMENT.measure.dcg10.logBase, 'Normalize', false, 'FixNumberRetrievedDocuments', runLength, 'FixedNumberRetrievedDocumentsPaddingStrategy', EXPERIMENT.measure.dcg10.fixedNumberRetrievedDocumentsPaddingStrategy);
EXPERIMENT.measure.dcg10.myCompute = @(runs) fast_dcg(runs, EXPERIMENT.measure.dcg10.logBase);

% Configuration for DCG with log base 10
EXPERIMENT.measure.ndcg10.id = 'ndcg10';
EXPERIMENT.measure.ndcg10.acronym = 'nDCG_b10';
EXPERIMENT.measure.ndcg10.name = 'Normalized Discounted Cumulated Gain';
EXPERIMENT.measure.ndcg10.logBase = 10;
EXPERIMENT.measure.ndcg10.fixedNumberRetrievedDocumentsPaddingStrategy = 'NotRelevant';
EXPERIMENT.measure.ndcg10.compute = @(pool, runSet, runLength) discountedCumulatedGain(pool, runSet, 'CutOffs', runLength, 'LogBase', EXPERIMENT.measure.ndcg10.logBase, 'Normalize', true, 'FixNumberRetrievedDocuments', runLength, 'FixedNumberRetrievedDocumentsPaddingStrategy', EXPERIMENT.measure.ndcg10.fixedNumberRetrievedDocumentsPaddingStrategy);
EXPERIMENT.measure.ndcg10.myCompute = @(runs) fast_dcg(runs, EXPERIMENT.measure.ndcg10.logBase);

% Configuration for CG 
EXPERIMENT.measure.cg.id = 'cg';
EXPERIMENT.measure.cg.acronym = 'CG';
EXPERIMENT.measure.cg.name = 'Cumulated Gain';
EXPERIMENT.measure.cg.fixedNumberRetrievedDocumentsPaddingStrategy = 'NotRelevant';
EXPERIMENT.measure.cg.compute = NaN;
EXPERIMENT.measure.cg.myCompute = @(runs) fast_cg(runs);


% Configuration for IDX
EXPERIMENT.measure.idx.id = 'idx';
EXPERIMENT.measure.idx.acronym = 'IDX';
EXPERIMENT.measure.idx.name = 'Index Measure';
EXPERIMENT.measure.idx.compute = @(pool, runSet, runLength) idxMeasure(pool, runSet, 'FixNumberRetrievedDocuments', runLength);

% Configuration for RB
EXPERIMENT.measure.rb.id = 'rb';
EXPERIMENT.measure.rb.acronym = 'RB';
EXPERIMENT.measure.rb.name = 'Recall Base';
EXPERIMENT.measure.rb.compute = @(pool, runSet, runLength) recallBase(pool, runSet);

% Returns the identifier of a measure given its index in EXPERIMENT.measure.list
EXPERIMENT.measure.getID = @(idx) ( EXPERIMENT.measure.(EXPERIMENT.measure.list{idx}).id ); 

% Returns the acronym of a measure given its index in EXPERIMENT.measure.list
EXPERIMENT.measure.getAcronym = @(idx) ( EXPERIMENT.measure.(EXPERIMENT.measure.list{idx}).acronym ); 

% Returns the name of a measure given its index in EXPERIMENT.measure.list
EXPERIMENT.measure.getName = @(idx) ( EXPERIMENT.measure.(EXPERIMENT.measure.list{idx}).name ); 


%% Configuration for breaking ties

% The list of possible ways of breaking ties
EXPERIMENT.ties.list = {'unq'}; %{'min', 'mid', 'max', 'unq'};
EXPERIMENT.ties.number = length(EXPERIMENT.ties.list);


%% Configuration for analyses

% The significance level for the analyses
EXPERIMENT.analysis.alpha.threshold = 0.05;
EXPERIMENT.analysis.alpha.color = 'lightgrey';

% Labels
EXPERIMENT.analysis.label.subject = 'Topic';
EXPERIMENT.analysis.label.factorA = 'System';

% Colors
EXPERIMENT.analysis.color.subject = rgb('FireBrick');
EXPERIMENT.analysis.color.factorA = rgb('RoyalBlue');

% Configuration for Kendall's tau correlation
EXPERIMENT.analysis.tau.id = 'tau';
EXPERIMENT.analysis.tau.latex.symbol = '$\tau$';
EXPERIMENT.analysis.tau.latex.label = 'Kendall''s $\tau$ Correlation';
EXPERIMENT.analysis.tau.name = 'Kendall''s tau correlation';
EXPERIMENT.analysis.tau.compute = @(x, y) corr(x, y, 'type', 'Kendall');
EXPERIMENT.analysis.tau.round = 8;

% Sign test
EXPERIMENT.analysis.sgn.id = 'sgn';
EXPERIMENT.analysis.sgn.name = 'Sign Test';
EXPERIMENT.analysis.sgn.compute = @(run1, run2) signtest(run1, run2);

% Wilcoxon Signed Rank test
EXPERIMENT.analysis.sgnrnk.id = 'sgnrnk';
EXPERIMENT.analysis.sgnrnk.name = 'Wilcoxon Signed Rank Test';
EXPERIMENT.analysis.sgnrnk.compute = @(run1, run2) signrank(run1, run2);

% Wilcoxon Rank Sum test
EXPERIMENT.analysis.rnksum.id = 'rnksum';
EXPERIMENT.analysis.rnksum.name = 'Wilcoxon Rank Sum Test';
EXPERIMENT.analysis.rnksum.compute = @(run1, run2) ranksum(run1, run2);

% Wilcoxon Rank Sum test
EXPERIMENT.analysis.t.id = 't';
EXPERIMENT.analysis.t.name = 'Student''s t Test';
EXPERIMENT.analysis.t.compute = @(run1, run2) ttest(run1, run2);

% One-way ANOVA for the System Effect 
EXPERIMENT.analysis.anova1.id = 'anova1';
EXPERIMENT.analysis.anova1.name = 'One-way ANOVA for the System Effect';
EXPERIMENT.analysis.anova1.compute = @(data) anova1(data, [], 'off');

% Kruskal-Wallis Test for the System Effect
EXPERIMENT.analysis.kw.id = 'kw';
EXPERIMENT.analysis.kw.name = 'Kruskal-Wallis Test for the System Effect';
EXPERIMENT.analysis.kw.compute = @(data) kruskalwallis(data, [], 'off');

% Two-way ANOVA for the Topic and System Effects
EXPERIMENT.analysis.anova2.id = 'anova2';
EXPERIMENT.analysis.anova2.name = 'Two-way ANOVA for the Topic and System Effects';
EXPERIMENT.analysis.anova2.compute = @(data) anova2(data, 1, 'off');

% Friedman Test for the Topic and System Effects
EXPERIMENT.analysis.f.id = 'f';
EXPERIMENT.analysis.f.name = 'Friedman Test for the Topic and System Effects';
EXPERIMENT.analysis.f.compute = @(data) friedman(data, 1, 'off');

% Tukey HSD multiple comparison analyses
EXPERIMENT.analysis.multcompare.system = @(sts) multcompare(sts, 'CType', 'hsd', 'Alpha', EXPERIMENT.analysis.alpha.threshold, 'Display', 'off');
