%% fast_dcg
% 
% Computes discounted cumulated gain (DCG) for a whole set of runs.

%% Synopsis
%
%   [m] = fast_dcg(runs, logBase)
%  
%
% *Parameters*
%
% * *|runs|* - the input run as a matrix of |0| (not relevant) and |1|
% relevant; each row is a different run; the left-most column is the
% top-ranked document, the right-most column is the bottom-ranked document.
% * *|logBase|* - the base of the logarithm. Set it to |2| for using a
% |log2|; any other values corresponds to |log10|.
%
%
% *Returns*
%
% * |m|  - a vector containing the value of the measure; each element
% corresponds to a different run.
%
%
%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2017a or higher
% * *Copyright:* (C) 2018-2021 <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>
%
%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

%%
function [m] = fast_dcg(runs, logBase)

    narginchk(2, 2);

    if (logBase ~= 2)
        logBase = 10;
    end
    
    % the length of a run
    N = size(runs, 2);
    
    % the total number of runs
    R = size(runs, 1);

    % the discount for the rank positions where no logarithmic smooting is 
    % required (repeated for each run)
    discount = ones(R, min(N, logBase));
    
    % the discount for the rank positions where the logarithmic smooting is 
    % required (repeated for each run)
    % append it to the discount without smoothing
    if (N > logBase)
        if (logBase == 2)     % impatient user
            discount = [discount repmat(log2(logBase+1:N), R, 1)];            
        else % patient user
            discount = [discount repmat(log10(logBase+1:N), R, 1)];
        end
    end
        
    m = sum(runs ./ discount, 2);            
end