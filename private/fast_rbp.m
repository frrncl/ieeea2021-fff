%% fast_rbp
% 
% Computes rank-biased precision (RBP) for a whole set of runs.

%% Synopsis
%
%   [m] = fast_rbp(runs, p)
%  
% It assumes binary relevance, otherwise it will not work properly.
%
% *Parameters*
%
% * *|runs|* - the input run as a matrix of |0| (not relevant) and |1|
% relevant; each row is a different run; the left-most column is the
% top-ranked document, the right-most column is the bottom-ranked document.
% * *|p|* - the persistence parameter.
%
%
% *Returns*
%
% * |m|  - a vector containing the value of the measure; each element
% corresponds to a different run.
%
%
%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2017a or higher
% * *Copyright:* (C) 2018-2021 <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>
%
%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

%%
function [m] = fast_rbp(runs, p)

    narginchk(2, 2);

    assert(p > 0 && p < 1, 'The persistence p must be in (0, 1).');

    % the length of a run
    N = size(runs, 2);

    % the total number of runs
    R = size(runs, 1);

    % all the possible rank positions, repeated for each run
    ranks = repmat(1:N, R, 1);
    
    % keep only the rank positions where a relevant document has been 
    % retrieved and switch to 0-based indexing which is what is expected in
    % the exponent of RBP.
    % Note that now positions of not relevant documents are set to -1
    ranks = ranks .* runs - 1;
        
    % in the case of not relevant documents p.*runs is 0 and ranks is - 1. 
    % Therefore, we have 0^-1 which gives Inf
    m = (p.*runs) .^ ranks;
    
    % transform the undesired Inf into NaN so that it is easy to omit them
    % from the subsequent sum
    m(isinf(m)) = NaN;
    
    % sum-up to obtain rank-biased precision by omitting undesired NaN
    m = (1 - p) .* sum(m, 2, 'omitnan');
       
end