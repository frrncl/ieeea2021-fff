%% fast_rr
% 
% Computes reciprocal rank (RR) for a whole set of runs.

%% Synopsis
%
%   [m] = fast_rr(runs)
%  
% *Parameters*
%
% * *|runs|* - the input run as a matrix of |0| (not relevant) and |1|
% relevant; each row is a different run; the left-most column is the
% top-ranked document, the right-most column is the bottom-ranked document.
%
%
% *Returns*
%
% * |m|  - a vector containing the value of the measure; each element
% corresponds to a different run.
%
%
%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2017a or higher
% * *Copyright:* (C) 2018-2021 <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>
%
%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

%%
function [m] = fast_rr(runs)

    narginchk(1, 1);

    % max(runs, [], 2) finds the maximum element of each row which is
    % either a logical 0 or a logical 1
    %
    % sel contains the above maximum which is a logical 0 for rows not
    % containing any relevant document and a logical 1 for rows containing
    % at least one relevant document. Basically, it is a vector indicating
    % which rows contain at least one relevant document
    %
    % m contains the index, row-by-row, of the first maximum element, that
    % is the index of the first relevant retrieved document for rows
    % retrieving at least one relevant document and 1 for the rows not
    % retrieving any relevant document, i.e. the position of the first 0.
    [sel, m] = max(runs, [], 2);
    
    % reciprocal rank is the inverse of the above first max element indices
    % for the rows retrieving at least one relevant document and it has to
    % be set to 0 for rows not retrieving any relevent document
    m = 1./m;
    m(~sel) = 0;             
end