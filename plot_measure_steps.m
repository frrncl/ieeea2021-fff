%% plot_measure_steps
% 
% Plots the Hasse diagram for the several measures. It generates Figure 2
% of the paper.
%
%
%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2017a or higher
% * *Copyright:* (C) 2018-2021 <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>
%
%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

%%
clear all;
close all;

common_parameters

fprintf('\n\n######## Plotting the Hasse diagram of various measures (%s) ########\n\n', EXPERIMENT.label.paper);

 % start of the plot
startPlot = tic;

% length of the runs
N = 4;

% generate all the possible runs of length N
runs = rem(floor((0:pow2(N)-1).' * pow2(1-N:0)), 2);

% Precision
fprintf('+ Precision\n');
m = sum(runs, 2)/N;
h = plot_measure_hasse(runs, m, 'Precision (and Recall)');
print(h, '-dpdf', EXPERIMENT.pattern.file.figure('general', 'p_steps.pdf'));
close(h);


% Average Precision
fprintf('+ Average Precision\n');
m = fast_ap(runs)/N;
h = plot_measure_hasse(runs, m, 'Average Precision');
ax = gca;
ylim = ax.YLim;
print(h, '-dpdf', EXPERIMENT.pattern.file.figure('general', 'ap_steps'));
close(h);


% Reciprocal Rank
fprintf('+ Reciprocal Rank\n');
m = fast_rr(runs);
h = plot_measure_hasse(runs, m, 'Reciprocal Rank');
print(h, '-dpdf', EXPERIMENT.pattern.file.figure('general', 'rr_steps'));
close(h);


% Discounted Cumulated Gain, log base = 2
fprintf('+ Discounted Cumulated Gain, log base = 2\n');
m = fast_dcg(runs, 2);
h = plot_measure_hasse(runs, m, 'Discounted Cumulated Gain, log base = 2');
print(h, '-dpdf', EXPERIMENT.pattern.file.figure('general', 'dcg02_steps'));
close(h);

% Discounted Cumulated Gain, log base = 10
fprintf('+ Discounted Cumulated Gain, log base = 10\n');
m = fast_dcg(runs, 10);
h = plot_measure_hasse(runs, m, 'Discounted Cumulated Gain, log base = 10');
print(h, '-dpdf', EXPERIMENT.pattern.file.figure('general', 'dcg10_steps'));
close(h);

% Rank-biased Precision, p = 0.2
fprintf('+ Rank-biased Precision, p = 0.2\n');
m = fast_rbp(runs, 0.2);
h = plot_measure_hasse(runs, m, 'Rank-biased Precision, p = 0.2');
ax = gca;
ax.YLim = ylim;
print(h, '-dpdf', EXPERIMENT.pattern.file.figure('general', 'rbp02_steps'));
close(h);

% Rank-biased Precision, p = 0.3
fprintf('+ Rank-biased Precision, p = 0.3\n');
m = fast_rbp(runs, 0.3);
h = plot_measure_hasse(runs, m, 'Rank-biased Precision, p = 0.3');
ax = gca;
ax.YLim = ylim;
print(h, '-dpdf', EXPERIMENT.pattern.file.figure('general', 'rbp03_steps'));
close(h);

% Rank-biased Precision, p = 0.5
fprintf('+ Rank-biased Precision, p = 0.5\n');
m = fast_rbp(runs, 0.5);
h = plot_measure_hasse(runs, m, 'Rank-biased Precision, p = 0.5');
ax = gca;
ax.YLim = ylim;
print(h, '-dpdf', EXPERIMENT.pattern.file.figure('general', 'rbp05_steps'));
close(h);

% Rank-biased Precision, p = 0.8
fprintf('+ Rank-biased Precision, p = 0.8\n');
m = fast_rbp(runs, 0.8);
h = plot_measure_hasse(runs, m, 'Rank-biased Precision, p = 0.8');
ax = gca;
ax.YLim = ylim;
print(h, '-dpdf', EXPERIMENT.pattern.file.figure('general', 'rbp08_steps'));
close(h);

fprintf('\n\n######## Total elapsed time for plotting the Hasse diagram of various measures (%s): %s ########\n\n', ...
    EXPERIMENT.label.paper, elapsedToHourMinutesSeconds(toc(startPlot)));




