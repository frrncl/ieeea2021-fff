%% plot_acr_steps2
% 
% Plots all the possible values of a measure for runs of a given length and
% saves the plot to a PDF file.
%
%% Synopsis
%
%   [] = plot_acr_steps2(tag, varargin)
%  
% *Parameters*
%
% * *|tag|* - the tag to be used in the file name.
% * *|varargin|* - the identifiers of the measures to plot
%
% *Returns*
%
% Nothing
%

%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2017a or higher
% * *Copyright:* (C) 2018-2021 <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>


%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}


%%
function [] = plot_acr_steps2(tag, varargin)

    persistent N
    
    if isempty(N)
        % length of the runs
        N = [5 10 20];
    end
  
    % check the number of input parameters
    narginchk(2, inf);
    
    % check that tag is a non-empty string
    validateattributes(tag, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'tag');
    
    if iscell(tag)
        % check that tag is a cell array of strings with one element
        assert(iscellstr(tag) && numel(tag) == 1, ...
            'MATTERS:IllegalArgument', 'Expected tag to be a cell array of strings containing just one string.');
    end
    
    common_parameters
    
    M = NaN(1, length(varargin));
    
    for i = 1:length(M)        
        validateattributes(varargin{i}, {'numeric'}, ...
            {'nonempty', 'integer', 'scalar', '>=', 1, '<=', EXPERIMENT.measure.number}, '', 'varargin');
        
        M(i) = varargin{i};
    end
    
    % start of overall computations
    startComputation = tic;
    
    fprintf('\n\n######## Plotting all the possible measure values (%s) ########\n\n', EXPERIMENT.label.paper);
    
    h = figure('Visible', 'off');

    t = tiledlayout(length(M),length(N), 'TileSpacing', 'Compact');

    % for each measure
    for i = 1:length(M)

        fprintf('+ %s\n', EXPERIMENT.measure.getAcronym(M(i)));

        % for each run length
        for j = 1:length(N)

            fprintf('  - N = %d\n', N(j));

            ax = nexttile;

            mid = EXPERIMENT.measure.list{M(i)};

            measureID = EXPERIMENT.pattern.identifier.acr.measure(mid, N(j));

            serload2(EXPERIMENT.pattern.file.measure(EXPERIMENT.track.ACR.id, measureID), ...
                'WorkspaceVarNames', {'measure'}, ...
                'FileVarNames', {measureID});

            % plot the measure values in increasing order of the measure, 
            % i.e. the runs on the X-axis are ordered in a possibly 
            % different way in each tile
            x =  1:2^N(j);
            
            plot(x, sort(measure), 'b.');

            clear measure

            if (i == 1)
                title(sprintf('N = %d', N(j)));
            end

            if (j == 1)
                ylabel(strrep(EXPERIMENT.measure.getAcronym(M(i)), '_', '\_'))
            end
            
            

            ax.XLim = [1 2^N(j)];
            ax.XTick =  round(prctile(x, [0 10 20 30 40 50 60 70 80 90 100]));
            ax.XTickLabel = {'0%', '10%', '20%', '30%', '40%', '50%', '60%', '70%', '80%', '90%', '100%'};
            
            ax.YLim = [0 max(1, ax.YLim(2))];
            
            if(ax.YLim(2) == 1)
                ax.YTick = 0:0.1:1;                
            end
            
        end % for run length
    end % for measure


    xlabel(t, 'Runs')

    fprintf('+ Saving \n');

    h.PaperPositionMode = 'auto';
    h.PaperUnits = 'centimeters';
    h.PaperSize = [length(N)*12 + 2 length(M)*9 + 2];
    h.PaperPosition = [1 1 length(N)*12 length(M)*9];


    print(h, '-dpdf', EXPERIMENT.pattern.file.figure('general', ['acr2_' tag '_steps']));
    close(h);


    fprintf('\n\n######## Total elapsed time for plotting tall the possible measure values (%s): %s ########\n\n', ...
        EXPERIMENT.label.paper, elapsedToHourMinutesSeconds(toc(startComputation)));
end



