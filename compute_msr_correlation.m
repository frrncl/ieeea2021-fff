%% compute_msr_correlation
% 
% Computes the overall correlation among measures for the given track, both
% between the original measures and between their ranked version,
% and saves them to a |.mat| file.
%
%% Synopsis
%
%   [] = compute_msr_correlation(trackID)
%  
%
% *Parameters*
%
% * *|trackID|* - the identifier of the track to process.
%
% *Returns*
%
% Nothing
%

%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2017a or higher
% * *Copyright:* (C) 2018-2021 <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>


%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

function [] = compute_msr_correlation(trackID)

    persistent MEASURES
    
    if isempty(MEASURES)
        MEASURES = [ 8  3; ... p vs rbp05
                     8  7; ... p vs rr
                     8  2; ... p vs rbp03
                     8  4; ... p vs rbp08
                     8  5; ... p vs dcg
                     8 10; ... p vs dcg10
                     3  2; ... rbp05 vs rbp03
                     3  4; ... rbp05 vs rbp08
                     3  7; ... rbp05 vs rr
                     3  5; ... rbp05 vs dcg
                     3 10; ... rbp05 vs dcg10
                     2  4; ... rbp03 vs rbp08
                     2  7; ... rbp03 vs rr
                     2  5; ... rbp03 vs dcg
                     2 10; ... rbp03 vs dcg10
                     4  7; ... rbp08 vs rr
                     4  5; ... rbp08 vs dcg
                     4 10; ... rbp08 vs dcg10
                     7  5; ... rr vs dcg
                     7 10; ... rr vs dcg10
                     5 10; ... dcg vs dcg10
                     9  8; ... r vs p
                     9  3; ... r vs rbp05
                     9  2; ... r vs rbp03
                     9  4; ... r vs rbp08
                     9  7; ... r vs rr
                     9  5; ... r vs dcg
                     9 10; ... r vs dcg10
                     9  1; ... r vs ap
                     9  6; ... r vs ndcg
                     9 11; ... r vs ndcg10
                     1  8; ... ap vs p
                     1  3; ... ap vs rbp05
                     1  2; ... ap vs rbp03
                     1  4; ... ap vs rbp08
                     1  7; ... ap vs rr
                     1  5; ... ap vs dcg
                     1 10; ... ap vs dcg10
                     1  6; ... ap vs ndcg
                     1 11; ... ap vs ndcg10
                     6  8; ... ndcg vs p
                     6  3; ... ndcg vs rbp05
                     6  2; ... ndcg vs rbp03
                     6  4; ... ndcg vs rbp08
                     6  7; ... ndcg vs rr
                     6  5; ... ndcg vs dcg
                     6 10; ... ndcg vs dcg10
                     6 11; ... ndcg vs ndcg10
                    11  8; ... ndcg10 vs p
                    11  3; ... ndcg10 vs rbp05
                    11  2; ... ndcg10 vs rbp03
                    11  4; ... ndcg10 vs rbp08
                    11  7; ... ndcg10 vs rr
                    11  5; ... ndcg10 vs dcg
                    11 10; ... ndcg10 vs dcg10
                   ];             
    end

   
    % check the number of input arguments
    narginchk(1, 1);

    % setup common parameters
    common_parameters;
        
    % check that trackID is a non-empty string
    validateattributes(trackID, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'trackID');

     if iscell(trackID)
        % check that trackID is a cell array of strings with one element
        assert(iscellstr(trackID) && numel(trackID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected trackID to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    trackID = char(strtrim(trackID));
    trackID = trackID(:).';
    
    % check that trackID assumes a valid value
    validatestring(trackID, ...
        EXPERIMENT.track.list, '', 'trackID');
         % start of overall computations
    startComputation = tic;

    fprintf('\n\n######## Computing correlation among measures on track %s (%s) ########\n\n', ...
        EXPERIMENT.track.(trackID).name, EXPERIMENT.label.paper);

    fprintf('+ Settings\n');
    fprintf('  - computed on %s\n\n', datestr(now, 'yyyy-mm-dd at HH:MM:SS'));
                
    
    % for each type of tie breaking
    for tt = 1:EXPERIMENT.ties.number
        ties = EXPERIMENT.ties.list{tt};
        
        fprintf('+ Breaking ties with %s\n', ties);
        
        tau.labels = cell(length(MEASURES), 2);
        
        % the correlation among the ordinary measures
        tau.values.ordinary = NaN(1, length(MEASURES));
        
        % the correlation among the ranked measures
        tau.values.ranked = NaN(1, length(MEASURES));
        
         
        % for each measure
        for m = 1:length(MEASURES)
            
            start = tic;
            
            tau.labels{m, 1} = EXPERIMENT.measure.getAcronym(MEASURES(m, 1));
            tau.labels{m, 2} = EXPERIMENT.measure.getAcronym(MEASURES(m, 2));
            
            fprintf('  - analysing %s vs %s\n', EXPERIMENT.measure.getAcronym(MEASURES(m, 1)), EXPERIMENT.measure.getAcronym(MEASURES(m, 2)));
            
            mid = EXPERIMENT.measure.list{MEASURES(m, 1)};            
            measureID = EXPERIMENT.pattern.identifier.measure(mid, trackID);
            
            serload2(EXPERIMENT.pattern.file.measure(trackID, measureID), ...
                'WorkspaceVarNames', {'msr1'}, ...
                'FileVarNames', {measureID});
            
            measureID = EXPERIMENT.pattern.identifier.rankMeasure(mid, ties, trackID);
            
            serload2(EXPERIMENT.pattern.file.measure(trackID, measureID), ...
                'WorkspaceVarNames', {'rnkmsr1'}, ...
                'FileVarNames', {measureID});
            
            % ensure that the systems are in the same order (it should already
            % be the case)
            rnkmsr1 = rnkmsr1(:, msr1.Properties.VariableNames);
            
            
            mid = EXPERIMENT.measure.list{MEASURES(m, 2)};            
            measureID = EXPERIMENT.pattern.identifier.measure(mid, trackID);
            
            serload2(EXPERIMENT.pattern.file.measure(trackID, measureID), ...
                'WorkspaceVarNames', {'msr2'}, ...
                'FileVarNames', {measureID});
            
            measureID = EXPERIMENT.pattern.identifier.rankMeasure(mid, ties, trackID);
            
            serload2(EXPERIMENT.pattern.file.measure(trackID, measureID), ...
                'WorkspaceVarNames', {'rnkmsr2'}, ...
                'FileVarNames', {measureID});
            
            % ensure that the systems are in the same order (it should already
            % be the case)
            rnkmsr2 = rnkmsr2(:, msr2.Properties.VariableNames);
            
            
            % compute the correlation among the ordinary measures
            tau.values.ordinary(m) = EXPERIMENT.analysis.tau.compute(round(mean(msr1{:, :}).', EXPERIMENT.analysis.tau.round), round(mean(msr2{:, :}).', EXPERIMENT.analysis.tau.round));
            
            % compute the correlation among the ranked measures
            tau.values.ranked(m) = EXPERIMENT.analysis.tau.compute(round(mean(rnkmsr1{:, :}).', EXPERIMENT.analysis.tau.round), round(mean(rnkmsr2{:, :}).', EXPERIMENT.analysis.tau.round));
            
            
            clear msr1 rnkmsr1 msr2 rnkmsr2
        end
        
        tauID = EXPERIMENT.pattern.identifier.tau.msr(trackID, ties);
        
        sersave2(EXPERIMENT.pattern.file.analysis(trackID, tauID), ...
            'WorkspaceVarNames', {'tau'}, ...
            'FileVarNames', {tauID});
        
        clear tau        
        
        fprintf('  - elapsed time: %s\n', elapsedToHourMinutesSeconds(toc(start)));
        
    end % for ties
    
           
    fprintf('\n\n######## Total elapsed time for computing correlation on track %s (%s): %s ########\n\n', ...
            EXPERIMENT.track.(trackID).name, EXPERIMENT.label.paper, elapsedToHourMinutesSeconds(toc(startComputation)));
end
