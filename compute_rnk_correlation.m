%% compute_rnk_correlation
% 
% Computes both the topic-by-topic and the overall correlation between a
% measure and its respectice ranked version for the given track and saves 
% them to a |.mat| file.
%
%% Synopsis
%
%   [] = compute_tpc_correlation(trackID, startMeasure, endMeasure)
%  
%
% *Parameters*
%
% * *|trackID|* - the identifier of the track to process.
% * *|startMeasure|* - the index of the start measure to process. Optional.
% * *|endMeasure|* - the index of the end measure to process. Optional.
%
% *Returns*
%
% Nothing
%

%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2017a or higher
% * *Copyright:* (C) 2018-2021 <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>


%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

function [] = compute_rnk_correlation(trackID, startMeasure, endMeasure)
   
    % check the number of input arguments
    narginchk(1, 3);

    % setup common parameters
    common_parameters;
        
    % check that trackID is a non-empty string
    validateattributes(trackID, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'trackID');

     if iscell(trackID)
        % check that trackID is a cell array of strings with one element
        assert(iscellstr(trackID) && numel(trackID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected trackID to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    trackID = char(strtrim(trackID));
    trackID = trackID(:).';
    
    % check that trackID assumes a valid value
    validatestring(trackID, ...
        EXPERIMENT.track.list, '', 'trackID');
    
    if nargin == 3
        validateattributes(startMeasure, {'numeric'}, ...
            {'nonempty', 'integer', 'scalar', '>=', 1, '<=', EXPERIMENT.measure.number }, '', 'startMeasure');
        
        validateattributes(endMeasure, {'numeric'}, ...
            {'nonempty', 'integer', 'scalar', '>=', startMeasure, '<=', EXPERIMENT.measure.number }, '', 'endMeasure');
    else 
        startMeasure = 1;
        endMeasure = EXPERIMENT.measure.number - EXPERIMENT.measure.helperNumber;
    end    
        
    % start of overall computations
    startComputation = tic;

    fprintf('\n\n######## Computing correlation among measures on track %s (%s) ########\n\n', ...
        EXPERIMENT.track.(trackID).name, EXPERIMENT.label.paper);

    fprintf('+ Settings\n');
    fprintf('  - computed on %s\n\n', datestr(now, 'yyyy-mm-dd at HH:MM:SS'));
    fprintf('  - slice \n');
    fprintf('    * start measure: %d (%s)\n', startMeasure, EXPERIMENT.measure.getAcronym(startMeasure));
    fprintf('    * end measure: %d (%s)\n\n', endMeasure, EXPERIMENT.measure.getAcronym(endMeasure));
                
    
    % for each type of tie breaking
    for tt = 1:EXPERIMENT.ties.number
        ties = EXPERIMENT.ties.list{tt};
        
        fprintf('+ Breaking ties with %s\n', ties);
        
        tau.labels = cell(1, endMeasure-startMeasure+1);
        
        % the correlation over the topic averages (standard way to compute it)
        tau.overall.values = NaN(1, endMeasure-startMeasure+1);
        
        % the correlation values topic-by-topic
        tau.topics.values = NaN(EXPERIMENT.track.(trackID).topics, endMeasure-startMeasure+1);
        tau.topics.mean = NaN;
        tau.topics.std = NaN;
        
        % for each measure
        for m = startMeasure:endMeasure
            
            start = tic;
            
            tau.labels{m} = EXPERIMENT.measure.getAcronym(m);
            
            fprintf('  - analysing %s\n', EXPERIMENT.measure.getAcronym(m));
            
            mid = EXPERIMENT.measure.list{m};
            
            measureID = EXPERIMENT.pattern.identifier.measure(mid, trackID);
            
            serload2(EXPERIMENT.pattern.file.measure(trackID, measureID), ...
                'WorkspaceVarNames', {'msr'}, ...
                'FileVarNames', {measureID});
            
            measureID = EXPERIMENT.pattern.identifier.rankMeasure(mid, ties, trackID);
            
            serload2(EXPERIMENT.pattern.file.measure(trackID, measureID), ...
                'WorkspaceVarNames', {'rnkmsr'}, ...
                'FileVarNames', {measureID});
            
            % ensure that the systems are in the same order (it should already
            % be the case)
            rnkmsr = rnkmsr(:, msr.Properties.VariableNames);
            
            % compute the overall correlation, i.e. the traditional one
            tau.overall.values(m) = EXPERIMENT.analysis.tau.compute(round(mean(msr{:, :}).', EXPERIMENT.analysis.tau.round), round(mean(rnkmsr{:, :}).', EXPERIMENT.analysis.tau.round));
            
            % compute the topic-by-topic correlation
            for t = 1:EXPERIMENT.track.(trackID).topics
                tau.topics.values(t, m) = EXPERIMENT.analysis.tau.compute(msr{t, :}.', rnkmsr{t, :}.');
            end
            
            clear msr rnkmsr
        end
        
        tau.topics.mean = mean(tau.topics.values);
        tau.topics.std = std(tau.topics.values);
        
        tauID = EXPERIMENT.pattern.identifier.tau.rnk(trackID, ties);
        
        sersave2(EXPERIMENT.pattern.file.analysis(trackID, tauID), ...
            'WorkspaceVarNames', {'tau'}, ...
            'FileVarNames', {tauID});
        
        clear tau        
        
        fprintf('  - elapsed time: %s\n', elapsedToHourMinutesSeconds(toc(start)));
        
    end % for ties
    
           
    fprintf('\n\n######## Total elapsed time for computing correlation on track %s (%s): %s ########\n\n', ...
            EXPERIMENT.track.(trackID).name, EXPERIMENT.label.paper, elapsedToHourMinutesSeconds(toc(startComputation)));
end
