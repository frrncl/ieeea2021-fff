%% trim_runs
% 
% Trims a set of runs to the length specificed in the overall configuration.
%
%% Synopsis
%
%   [] = trim_runs(inputTrackID, outputTrackID)
%  
%
% *Parameters*
%
% * *|inputTrackID|* - the identifier of the input track to process.
% * *|outputTrackID|* - the identifier of the output track to produce.
%
% *Returns*
%
% Nothing
%

%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2017a or higher
% * *Copyright:* (C) 2018-2021 <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

%%
function [] = trim_runs(inputTrackID, outputTrackID)

    persistent PROCESS_RUN;
    
    if isempty(PROCESS_RUN)        
        PROCESS_RUN = @processRun;
    end

    % check that we have the correct number of input arguments. 
    narginchk(2, 2);
    
    % setup common parameters
    common_parameters;
        
    % check that inputTrackID is a non-empty string
    validateattributes(inputTrackID,{'char', 'cell'}, {'nonempty', 'vector'}, '', 'inputTrackID');

    if iscell(inputTrackID)
        % check that inputTrackID is a cell array of strings with one element
        assert(iscellstr(inputTrackID) && numel(inputTrackID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected inputTrackID to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    inputTrackID = char(strtrim(inputTrackID));
    inputTrackID = inputTrackID(:).';
    
    % check that inputTrackID assumes a valid value
    validatestring(inputTrackID, ...
        EXPERIMENT.track.list, '', 'inputTrackID');    
    
    % check that outputTrackID is a non-empty string
    validateattributes(outputTrackID,{'char', 'cell'}, {'nonempty', 'vector'}, '', 'outputTrackID');

    if iscell(outputTrackID)
        % check that outputTrackID is a cell array of strings with one element
        assert(iscellstr(outputTrackID) && numel(outputTrackID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected outputTrackID to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    outputTrackID = char(strtrim(outputTrackID));
    outputTrackID = outputTrackID(:).';
    
    % check that outputTrackID assumes a valid value
    validatestring(outputTrackID, ...
        EXPERIMENT.track.list, '', 'outputTrackID');    
        
    % start of overall computations
    startComputation = tic;

    fprintf('\n\n######## Trimming runs for track %s (%s) ########\n\n', EXPERIMENT.track.(inputTrackID).name, EXPERIMENT.label.paper);

    fprintf('+ Settings\n');
    fprintf('  - computed on %s\n', datestr(now, 'yyyy-mm-dd at HH:MM:SS'));
    fprintf('  - runs with length %d of %s trimmed to runs with length %d of %s\n', ...
        EXPERIMENT.track.(inputTrackID).runLength, inputTrackID, ...
        EXPERIMENT.track.(outputTrackID).runLength, outputTrackID);


    fprintf('+ Loading the dataset\n\n');
    poolID = EXPERIMENT.pattern.identifier.pool(inputTrackID);
    
    serload2(EXPERIMENT.pattern.file.dataset(inputTrackID, poolID), ...
        'WorkspaceVarNames', {'pool'}, ...
        'FileVarNames', {poolID});
    
    runID =  EXPERIMENT.pattern.identifier.run(inputTrackID);
    
    serload2(EXPERIMENT.pattern.file.dataset(inputTrackID, runID), ...
        'WorkspaceVarNames', {'runSet'}, ...
        'FileVarNames', {runID});
    
    fprintf('+ Trimming runs\n');
    
    % replicate runLength to properly work with cellfun
    runLength = repmat({EXPERIMENT.track.(outputTrackID).runLength}, 1, width(runSet));
    
    for t = 1:height(runSet)
        runSet{t, :} = cellfun(PROCESS_RUN, runSet{t, :}, runLength, 'UniformOutput', false);
    end % for topic
    
    
    fprintf('+ Saving runs\n');
    
    runID =  EXPERIMENT.pattern.identifier.run(outputTrackID);
    runSet.Properties.UserData.identifier = runID;
    
    sersave2(EXPERIMENT.pattern.file.dataset(outputTrackID, runID), ...
        'WorkspaceVarNames', {'runSet'}, ...
        'FileVarNames', {runID});
    
    
    poolID = EXPERIMENT.pattern.identifier.pool(outputTrackID);
    pool.Properties.UserData.identifier = poolID;
    
    sersave2(EXPERIMENT.pattern.file.dataset(outputTrackID, poolID), ...
        'WorkspaceVarNames', {'pool'}, ...
        'FileVarNames', {poolID});

    fprintf('\n\n######## Total elapsed time for trimming runs for track %s (%s): %s ########\n\n', ...
        EXPERIMENT.track.(inputTrackID).name, EXPERIMENT.label.paper, elapsedToHourMinutesSeconds(toc(startComputation)));
    
end


function [runTopic] = processRun(topic, runLength)
   runTopic = topic(1:min(height(topic), runLength), :);
end
