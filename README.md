Matlab source code for running the experiments reported in the paper:

* Ferrante, M., Ferro, N., and Fuhr, N. (2021). Towards Meaningful Statements in IR Evaluation: Mapping Evaluation Measures to Interval Scales. _IEEE Access_.
