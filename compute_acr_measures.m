%% compute_acr_measures
% 
% Computes the ACR measures, i.e. measures computed on the all-cases runs
% that is all the possible runs of a given length using binary judgements,
% and saves them to a |.mat| file.
%
%% Synopsis
%
%   [] = compute_ac_measures(N, startMeasure, endMeasure)
%  
%
% *Parameters*
%
% * *|N|* - the length of the runs to generate.
% * *|startMeasure|* - the index of the start measure to process. Optional.
% * *|endMeasure|* - the index of the end measure to process. Optional.
%
% *Returns*
%
% Nothing
%

%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2017a or higher
% * *Copyright:* (C) 2018-2021 <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

function [] = compute_acr_measures(N, startMeasure, endMeasure)

    % check the number of input arguments
    narginchk(1, 3);

    % setup common parameters
    common_parameters;
        
    % check that N is a scalar integer greater than or equal to 1
    validateattributes(N, {'numeric'}, ...
            {'nonempty', 'integer', 'scalar', '>=', 1}, '', 'N');
    
    if nargin == 3
        validateattributes(startMeasure, {'numeric'}, ...
            {'nonempty', 'integer', 'scalar', '>=', 1, '<=', EXPERIMENT.measure.number }, '', 'startMeasure');
        
        validateattributes(endMeasure, {'numeric'}, ...
            {'nonempty', 'integer', 'scalar', '>=', startMeasure, '<=', EXPERIMENT.measure.number }, '', 'endMeasure');
    else 
        startMeasure = 1;
        endMeasure = EXPERIMENT.measure.number - EXPERIMENT.measure.helperNumber;
    end
            
    % start of overall computations
    startComputation = tic;

    fprintf('\n\n######## Computing ranks with respect to ACR measures (%s) ########\n\n', EXPERIMENT.label.paper);

    fprintf('+ Settings\n');
    fprintf('  - computed on %s\n', datestr(now, 'yyyy-mm-dd at HH:MM:SS'));
    fprintf('  - run length %d\n', N);
    fprintf('  - slice \n');
    fprintf('    * start measure: %d (%s)\n', startMeasure, EXPERIMENT.measure.getAcronym(startMeasure));
    fprintf('    * end measure: %d (%s)\n\n', endMeasure, EXPERIMENT.measure.getAcronym(endMeasure));    
            
    fprintf('+ Generating the ACR runs\n\n');
    
    runs = rem(floor((0:pow2(N)-1).' * pow2(1-N:0)), 2);
    
    % for each measure
    for m = startMeasure:endMeasure
        
        start = tic;
        
        fprintf('+ Processing %s\n', EXPERIMENT.measure.getAcronym(m));
        
        mid = EXPERIMENT.measure.list{m};
                
        fprintf('  - computing %s\n', EXPERIMENT.measure.getAcronym(m));
        
        % compute the measure for the ACR runs
        % CAVEATS: runs contains all the possible runs of length N.
        % When RB(i) < N, it contains also runs which retrieve more
        % relevant documents than the recall base, which would not
        % be possible. Computations are done also for those
        % impossible runs. However, when computing rank measures,
        % these runs are never picked up since they cannot exist 
        % among the actual runs for a given topic.
        measure =  EXPERIMENT.measure.(mid).myCompute(runs);
        
        measureID = EXPERIMENT.pattern.identifier.acr.measure(mid, N);
        
        fprintf('    * saving %s\n', EXPERIMENT.measure.getAcronym(m));
        
        sersave2(EXPERIMENT.pattern.file.measure(EXPERIMENT.track.ACR.id, measureID), ...
            'WorkspaceVarNames', {'measure'}, ...
            'FileVarNames', {measureID});
                
        % for each type of tie breaking
        for t = 1:EXPERIMENT.ties.number
            ties = EXPERIMENT.ties.list{t};
            
            fprintf('  - breaking ties with %s\n', ties);
            
            % determine the rank of each run with respect to the ordering
            % induced by the measure.
            % If two or more runs are tied, i.e. they have the same value
            % of the measure, break ties according to the requested
            % strategy.
            ranks = break_ties(measure, ties);

            ranksID = EXPERIMENT.pattern.identifier.acr.ranks(mid, ties, N);
            
            fprintf('    * saving %s ranks\n', ties);
            
            sersave2(EXPERIMENT.pattern.file.measure(EXPERIMENT.track.ACR.id, ranksID), ...
                'WorkspaceVarNames', {'ranks'}, ...
                'FileVarNames', {ranksID});
        end % for ties
        
        fprintf('  - elapsed time: %s\n', elapsedToHourMinutesSeconds(toc(start)));
        
        clear measure ranks;
        
    end % for measure
        
    fprintf('\n\n######## Total elapsed time for computing ranks with respect to ACR measures (%s): %s ########\n\n', ...
            EXPERIMENT.label.paper, elapsedToHourMinutesSeconds(toc(startComputation)));
end
