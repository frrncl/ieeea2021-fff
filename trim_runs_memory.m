%% trim_runs
% 
% Trims a set of runs to the length specificed.
%
%% Synopsis
%
%   [trimmedRunSet] = trim_runs_memory(runSet, threshold)
%  
%
% *Parameters*
%
% * *|runSet|* - the runs to be trimmed.
% * *|threshold|* - the cut-off.
%
% *Returns*
%
% Nothing
%

%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2017a or higher
% * *Copyright:* (C) 2018-2021 <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

%%
function [trimmedRunSet] = trim_runs_memory(runSet, threshold)

    persistent PROCESS_RUN;
    
    if isempty(PROCESS_RUN)        
        PROCESS_RUN = @processRun;
    end

    % check that we have the correct number of input arguments. 
    narginchk(2, 2);
        
    % replicate runLength to properly work with cellfun
    runLength = repmat({threshold}, 1, width(runSet));
    
    trimmedRunSet = runSet;
    
    for t = 1:height(runSet)
        trimmedRunSet{t, :} = cellfun(PROCESS_RUN, runSet{t, :}, runLength, 'UniformOutput', false);
    end % for topic
    
    trimmedRunSet.Properties.UserData.identifier =  sprintf('%s_%d', runSet.Properties.UserData.identifier, threshold);
        
end


function [runTopic] = processRun(topic, runLength)
   runTopic = topic(1:min(height(topic), runLength), :);
end
